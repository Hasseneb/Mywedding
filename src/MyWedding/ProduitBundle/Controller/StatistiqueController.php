<?php
/**
 * Created by PhpStorm.
 * User: mathlouthi
 * Date: 11/05/2017
 * Time: 22:00
 */

namespace MyWedding\ProduitBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class StatistiqueController extends Controller
{


    public function statUserAction()
    {
        $em=$this->getDoctrine()->getManager();

        $commande=$em->getRepository('SalesBundle:Cart')->findAll();
        $nbCommande = count($commande);

        $user=$em->getRepository('MyWeddingUserBundle:User')->findAll();
        $nbUser = count($user);

        $Promotion=$em->getRepository('MyWeddingProduitBundle:Promotion')->findAll();
        $list= $em->getRepository('MyWeddingProduitBundle:ListCouple')->findAll();
        $nbrList = count($list);
        $nbPromotion = count($Promotion);

        $commandeParMois=$em->getRepository('SalesBundle:CartSeller')->cmdParMois();

        for($i = 0; $i < count($commandeParMois); ++$i) {

            switch ($commandeParMois[$i]['month_x']) :
                case  1: $commandeParMois[$i]['month_x']= 'Janvier';break;
                case  2: $commandeParMois[$i]['month_x']= 'Février';break;
                case  3: $commandeParMois[$i]['month_x']= 'Mars';break;
                case  4: $commandeParMois[$i]['month_x']= 'Avril';break;
                case  5: $commandeParMois[$i]['month_x']= 'Mai';break;
                case  6: $commandeParMois[$i]['month_x']= 'Juin';break;
                case  7: $commandeParMois[$i]['month_x']= 'Juillet';break;
                case  8: $commandeParMois[$i]['month_x']= 'Août';break;
                case  9: $commandeParMois[$i]['month_x']= 'Septembre';break;
                case 10: $commandeParMois[$i]['month_x']= 'Octobre';break;
                case 11: $commandeParMois[$i]['month_x']= 'Novembre';break;
                case 12: $commandeParMois[$i]['month_x']= 'Décembre';break;
            endswitch;
        }


        return $this->render('MyWeddingProduitBundle:Statistique:statUser.html.twig', array('nbCommande' => $nbCommande ,'nbUser' => $nbUser,'nbPromotion' => $nbPromotion,'commandeParMois' => $commandeParMois ,'nbrList'=>$nbrList));

    }




}