<?php

namespace MyWedding\ProduitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MyWedding\ProduitBundle\Entity\Picture;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProduitBundle\Entity\Lequipe;
use MyWedding\ProduitBundle\Form\LequipeType;

/**
 * Lequipe controller.
 *
 */
class LequipeController extends Controller
{

    /**
     * Lists all Lequipe entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:Lequipe')->findAll();

        return $this->render('MyWeddingProduitBundle:Lequipe:index.html.twig', array(
            'entities' => $entities,
        ));
    }


    public function allequipeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:Lequipe')->findAll();

        return $this->render('MyWeddingProduitBundle:Lequipe:allequipe.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Lequipe entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Lequipe();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $picture = new Picture();
            $em = $this->getDoctrine()->getManager();
            $picture->upload();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lequipe_show', array('id' => $entity->getId())));
        }

        return $this->render('MyWeddingProduitBundle:Lequipe:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Lequipe entity.
     *
     * @param Lequipe $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Lequipe $entity)
    {
        $form = $this->createForm(new LequipeType(), $entity, array(
            'action' => $this->generateUrl('lequipe_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Lequipe entity.
     *
     */
    public function newAction()
    {
        $entity = new Lequipe();
        $form   = $this->createCreateForm($entity);

        return $this->render('MyWeddingProduitBundle:Lequipe:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Lequipe entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Lequipe')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lequipe entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:Lequipe:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Lequipe entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Lequipe')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lequipe entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:Lequipe:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Lequipe entity.
    *
    * @param Lequipe $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Lequipe $entity)
    {
        $form = $this->createForm(new LequipeType(), $entity, array(
            'action' => $this->generateUrl('lequipe_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Lequipe entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Lequipe')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lequipe entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('lequipe_edit', array('id' => $id)));
        }

        return $this->render('MyWeddingProduitBundle:Lequipe:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Lequipe entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingProduitBundle:Lequipe')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Lequipe entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('lequipe'));
    }

    /**
     * Creates a form to delete a Lequipe entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lequipe_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
