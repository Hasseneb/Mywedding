<?php

namespace MyWedding\ProduitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProduitBundle\Entity\Picture;
use MyWedding\ProduitBundle\Entity\SousCategory;
use MyWedding\ProduitBundle\Form\SousCategoryType;

/**
 * SousCategory controller.
 *
 */
class SousCategoryController extends Controller
{

    /**
     * Lists all SousCategory entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:SousCategory')->findAll();

        return $this->render('MyWeddingProduitBundle:SousCategory:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new SousCategory entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new SousCategory();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $picture = new Picture();

            $em = $this->getDoctrine()->getManager();
            $picture->upload();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('souscategory_show', array('id' => $entity->getId())));
        }

        return $this->render('MyWeddingProduitBundle:SousCategory:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a SousCategory entity.
     *
     * @param SousCategory $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SousCategory $entity)
    {
        $form = $this->createForm(new SousCategoryType(), $entity, array(
            'action' => $this->generateUrl('souscategory_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SousCategory entity.
     *
     */
    public function newAction()
    {
        $entity = new SousCategory();
        $form   = $this->createCreateForm($entity);

        return $this->render('MyWeddingProduitBundle:SousCategory:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SousCategory entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:SousCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SousCategory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:SousCategory:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SousCategory entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:SousCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SousCategory entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:SousCategory:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a SousCategory entity.
    *
    * @param SousCategory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SousCategory $entity)
    {
        $form = $this->createForm(new SousCategoryType(), $entity, array(
            'action' => $this->generateUrl('souscategory_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SousCategory entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:SousCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SousCategory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('souscategory_edit', array('id' => $id)));
        }

        return $this->render('MyWeddingProduitBundle:SousCategory:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a SousCategory entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingProduitBundle:SousCategory')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SousCategory entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('souscategory'));
    }

    /**
     * Creates a form to delete a SousCategory entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('souscategory_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
