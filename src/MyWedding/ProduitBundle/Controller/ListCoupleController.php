<?php

namespace MyWedding\ProduitBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use MyWedding\ProduitBundle\Entity\Adresse;
use MyWedding\ProduitBundle\Entity\DetailsProduit;
use MyWedding\ProduitBundle\Entity\Partenaire;
use MyWedding\ProduitBundle\Entity\Picture;
use MyWedding\ProduitBundle\Entity\Product;
use MyWedding\ProduitBundle\Form\ListCoupleProfileType;
use MyWedding\ProduitBundle\Form\PictureType;
use MyWedding\ProduitBundle\Form\searchType;
use MyWedding\UserBundle\Entity\User;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProduitBundle\Entity\ListCouple;
use MyWedding\ProduitBundle\Form\ListCoupleType;
use Symfony\Component\HttpFoundation\Response;

/**
 * ListCouple controller.
 *
 */
class ListCoupleController extends Controller
{

    /**
     * Lists all ListCouple entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:ListCouple')->findAll();

        return $this->render(
            'MyWeddingProduitBundle:ListCouple:index.html.twig',
            array(
                'entities' => $entities,
            )
        );
    }

    /**
     * Creates a new ListCouple entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new ListCouple();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $entity = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('listcouple_show', array('id' => $entity->getId())));
        }

        return $this->render(
            'MyWeddingProduitBundle:ListCouple:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }


    public function createListAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $liste = new ListCouple();
        $form = $this->createForm(new ListCoupleProfileType());
        $form->handleRequest($request);
        $errors = $form->getErrors(true);
        if ($form->isValid()) {
            $profil = new Picture();
            $profil = $form["profil"]->getData();
            if ($profil != null){
            $profil->preUpload();
            }
            $couverture = new Picture();
            $couverture = $form["couverture"]->getData();
            if ($couverture != null) {
                $couverture->preUpload();
            }
            if ($user->getAdresse() == null)
            {
                $adresse = new Adresse();
                $adresse->setAdress($form["adress"]->getData());
                $adresse->setVille($form["ville"]->getData());
                $adresse->setCodePostal($form["codePostal"]->getData());
                $adresse->setUser($user);
                $user->setAdresse($adresse);
            }
            else {
                $adresse = $user->getAdresse();
                $adresse->setAdress($form["adress"]->getData());
                $adresse->setVille($form["ville"]->getData());
                $adresse->setCodePostal($form["codePostal"]->getData());
            }

            $user->setFirstname($form["prenom"]->getData());
            $user->setLastname($form["nom"]->getData());
            $user->setNumero($form["numTel"]->getData());
            $user->setFirstNamePartenaire($form["prenomPartenaire"]->getData());
            $user->setLastNamePartenaire($form["nomPartenaire"]->getData());
            $user->setEmailPartenaire($form["emailPartenaire"]->getData());
            $liste->setName($form["lien"]->getData());
            $liste->setDateMariage($form["dateMariage"]->getData());
            $user->setListeCouple($liste);
            if ($profil != null){
                $liste->setPhotoDeProfil($profil);
            }
            if ($couverture != null){
                $liste->setPhotoDeCouverture($couverture);
            }
            $liste->setUser($user);
            $em = $this->getDoctrine()->getManager();
            $em->persist($adresse);
            $em->persist($user);
            $em->persist($liste);
            $em->flush();
            $this->addFlash('edit_list_success', 'Votre liste a été créer avec succés');

            if ($request->isXmlHttpRequest()) {
                return $this->render(
                    "@MyWeddingProduit/ListCouple/newList.html.twig",
                    array(
                        'form' => $form->createView(),
                    )
                );
            } else {
                return $this->redirectToRoute('homepage');
            }

        }

        return $this->render(
            "@MyWeddingProduit/ListCouple/newList.html.twig",
            array(
                'form' => $form->createView(),
                'errors' => $errors,
            )
        );


    }

    public function activerDesactiverMaListeAction()
    {
        $user = $this->getUser();
        if ($user->getListeCouple()->getIsEnabled() == true) {
            $user->getListeCouple()->setIsEnabled(false);
        } else {
            $user->getListeCouple()->setIsEnabled(true);
        }
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('my_wedding_produit_homepage');
    }

    /**
     * Controller that shows the list page (/liste/name) front
     */
    public function viewListAction($name)
    {
        $repository = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:ListCouple');
        $liste = $repository->findOneBy(array('name' => $name));
        if (!$liste) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }
        if (($this->getUser() == null) && $liste->getIsEnabled() == false) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }
        if ($this->getUser() != null) {
            if ($this->getUser()->getListeCouple() != null) {
                if (($liste->getName() != $this->getUser()->getListeCouple()->getName()) && $liste->getIsEnabled(
                    ) == false
                ) {
                    throw $this->createNotFoundException('Unable to find ListCouple entity.');
                }
            } elseif ($this->getUser()->getListeCouple() == null && $liste->getIsEnabled() == false) {
                throw $this->createNotFoundException('Unable to find ListCouple entity.');
            }
        }


        if ($this->getUser() == null && $liste->getIsEnabled() == false) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }

        $em = $this->getDoctrine()->getManager();
        $collectes = $liste->getCollecte();

        return $this->render(
            '@MyWeddingProduit/ListCouple/showListe.html.twig',
            [
                'liste' => $liste,
                'collectes' => $collectes,
                'name' => $name,
            ]
        );
    }


    public
    function getQuantiteWantedAction(
        $liste_id,
        $produit_id
    ) {
        $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findOneBy(array('listeCouple_id' => $liste_id, 'produit_id' => $produit_id));
        if ($detailsProduit) {
            return new Response($detailsProduit->getQuantiteVoulu());
        } else {
            return new Response('');
        }

    }

    public
    function getProduitAcheteOuNonAction(
        $liste_id,
        $produit_id
    ) {
        $detail = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findOneBy(array('listeCouple_id' => $liste_id, 'produit_id' => $produit_id));
        if ($detail->getQuantiteRestante() != 0) {
            return new Response('Demandé');
        } else {
            return new Response('Acheté');
        }

    }


    public
    function getPriceRangeAction(
        $liste_id,
        $produit_id
    ) {
        $detail = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findOneBy(array('listeCouple_id' => $liste_id, 'produit_id' => $produit_id));
        if ($detail->getPrixPiece() < 50) {
            return new Response('1');
        } elseif ($detail->getPrixPiece() >= 50 && $detail->getPrixPiece() < 150) {
            return new Response('2');
        } elseif ($detail->getPrixPiece() >= 150 && $detail->getPrixPiece() < 300) {
            return new Response('3');
        } elseif ($detail->getPrixPiece() >= 300) {
            return new Response('4');
        }


    }

    public
    function getPriceRangeCollectionAction(
        Product $detail
    ) {

        if ($detail->getPrix() < 50) {
            return new Response('1');
        } elseif ($detail->getPrix() >= 50 && $detail->getPrix() < 150) {
            return new Response('2');
        } elseif ($detail->getPrix() >= 150 && $detail->getPrix() < 300) {
            return new Response('3');
        } elseif ($detail->getPrix() >= 300) {
            return new Response('4');
        }


    }

    /**
     * Controller that renders the number of products wanted in a list
     */

    public
    function quantiteVoulueAction(
        $liste_id,
        $produit_id
    ) {
        $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findOneBy(array('listeCouple_id' => $liste_id, 'produit_id' => $produit_id));

        return $this->render(
            '@MyWeddingProduit/ListCouple/listeProduitQuantite.html.twig',
            [
                'produit' => $detailsProduit,
            ]
        );
    }
    function quantiteRestanteAction(
        $liste_id,
        $produit_id
    ) {
        $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findOneBy(array('listeCouple_id' => $liste_id, 'produit_id' => $produit_id));
        return new Response($detailsProduit->getQuantiteRestante());
    }


    public
    function changerQuantiteVouluAction(
        Request $request,
        $liste_id,
        $produit_id
    ) {
        $quantity = $request->request->get('quantity');
        if ($quantity != null) {
            $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
                ->findOneBy(array('listeCouple_id' => $liste_id, 'produit_id' => $produit_id));
            $detailsProduit->setQuantiteVoulu($quantity);
            $em = $this->getDoctrine()->getManager();
            $em->persist($detailsProduit);
            $em->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }

    public
    function listeCategorieAction(
        $liste_id
    ) {

        $liste = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:ListCouple')
            ->findOneBy(array('id' => $liste_id));
        $collections = $liste->getCollecte();
        $products = $liste->getProducts();
        $index = 0; // array index
        $categories = array();
        $produit = new Product();
        foreach ($products as $produit) {
            $key = in_array((string)$produit->getCategory(), array_column($categories, 'name'));
            if ($key) {
                $key = array_search((string)$produit->getCategory(), array_column($categories, 'name'));
                $categories[$key]['total'] = $categories[$key]['total'] + 1;
            } else {
                $categories[$index]['name'] = ((string)$produit->getCategory());
                $categories[$index]['total'] = 1;
                $index++;
            }
        }

        foreach ($collections as $collecte) {
            foreach ($collecte->getProduit() as $produit) {
                $key = in_array((string)$produit->getCategory(), array_column($categories, 'name'));
                if ($key) {
                    $key2 = array_search((string)$produit->getCategory(), array_column($categories, 'name'));
                    $categories[$key2]['total'] = $categories[$key]['total'] + 1;
                } else {
                    $categories[$index]['name'] = ((string)$produit->getCategory());
                    $categories[$index]['total'] = 1;
                    $index++;
                }
            }
        }

        return $this->render(
            '@MyWeddingProduit/ListCouple/categories_liste.html.twig',
            [
                'categories' => $categories,
            ]
        );


    }
// controle sur les produits de la liste ychouf 9Adech 3Andou men qty
//restante ken 0 techré ycid el acheté ou ken mezelou matechrwech yzid lel demander
    public
    function listeProduitsFilterAction(
        $liste_id
    ) {


        $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findBy(array('listeCouple_id' => $liste_id));
        $nbProduitDemande = 0;
        $nbProduitAchete = 0;
        foreach ($detailsProduit as $detail) {
            if ($detail->getQuantiteRestante() != 0) {
                $nbProduitDemande = $nbProduitDemande + 1;
            } else {
                $nbProduitAchete = $nbProduitAchete + 1;
            }

        }

        return $this->render(
            '@MyWeddingProduit/ListCouple/produitFilter.html.twig',
            [
                'nbDemande' => $nbProduitDemande,
                'nbAchete' => $nbProduitAchete,
            ]
        );


    }

    public
    function listeCollectionFilterAction(
        $liste_id
    ) {

        $liste = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:ListCouple')
            ->find($liste_id);
        $collections = $liste->getCollecte();
        $nbProduitDemande = 0;
        $nbProduitAchete = 0;
        foreach ($collections as $collection) {
            foreach ($collection->getProduit() as $produit) {
                if ($collection->getSommeAtteinte() != $produit->getPrix()) {
                    $nbProduitDemande = $nbProduitDemande + 1;
                } else {
                    $nbProduitAchete = $nbProduitAchete + 1;
                }

            }
        }

            return $this->render(
                '@MyWeddingProduit/ListCouple/CollectionFilter.html.twig',
                [
                    'nbDemande' => $nbProduitDemande,
                    'nbAchete' => $nbProduitAchete,
                ]
            );


        }


    public
    function PrixListeTotaleAction(
        $liste_id
    ) {
        $liste = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:ListCouple')
            ->find($liste_id);
        $collections = $liste->getCollecte();
        $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findBy(array('listeCouple_id' => $liste_id));
        $prix = 0;
        foreach ($collections as $collection) {
            foreach ($collection->getProduit() as $produit) {
                $prix = $prix + $produit->getPrix();

            }
        }
            foreach ($detailsProduit as $detail) {
                $prix = $prix + ($detail->getPrixPiece() * $detail->getQuantiteVoulu());
            }

            return $this->render(
                '@MyWeddingProduit/ListCouple/prixTotal.html.twig',
                [
                    'PrixListe' => $prix,
                ]
            );

        }



    public
    function PrixFilterAction(
        $liste_id
    ) {
        $liste = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:ListCouple')
            ->find($liste_id);
        $collections = $liste->getCollecte();
        $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findBy(array('listeCouple_id' => $liste_id));
        $filtre = [0, 0, 0, 0];
        if ($collections != null) {
           foreach ($collections as $collection) {
               foreach ($collection->getProduit() as $produit) {
                $prix = $produit->getPrix();
                if ($prix < 50) {
                    $filtre [0] += 1;
                } elseif ($prix >= 50 && $prix < 150) {
                    $filtre [1] += 1;
                } elseif ($prix >= 150 && $prix < 300) {
                    $filtre [2] += 1;
                } elseif ($prix >= 300) {
                    $filtre [3] += 1;
                }

            }
           }
        }
        if ($detailsProduit != null) {
            foreach ($detailsProduit as $detail) {
                $prix = $detail->getPrixPiece();
                if ($prix < 50) {
                    $filtre [0] += 1;
                } elseif ($prix >= 50 && $prix < 100) {
                    $filtre [1] += 1;
                } elseif ($prix >= 150 && $prix < 300) {
                    $filtre [2] += 1;
                } elseif ($prix >= 300) {
                    $filtre [3] += 1;
                }
            }
        }

        return $this->render(
            '@MyWeddingProduit/ListCouple/prixfiltre.html.twig',
            [
                'filtre' => $filtre,
            ]
        );


    }


    /**
     * Controller that shows the list page (/liste/name)
     */
    public
    function editListProfileAction(
        Request $request
    ) {
        $liste = new ListCouple();
        /** @var Form $editForm */
        $editForm = $this->createFormBuilder()
            ->add('name', 'text', array('required' => false))
            ->add(
                'dateMariage',
                'date',
                array(
                    'widget' => 'single_text',
                    'required' => false,
                )
            )
            ->add('isDateNotKnown', 'checkbox', array('label' => false, 'required' => false))
            ->getForm();

        $repository = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:ListCouple');
        $user = $this->getUser();
        $editForm->get('name')->setData($user->getListeCouple()->getName());
        if ($user->getListeCouple()->getDateMariage() != null) {
            $editForm->get('dateMariage')->setData($user->getListeCouple()->getDateMariage());
        }
        $liste = $repository->findOneBy(array('user' => $user));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($editForm["name"]->getData() != null) {
                $liste->setName($editForm["name"]->getData());
            }
            $isDateNotknown = $editForm["isDateNotKnown"]->getData();
            if ($isDateNotknown == true) {
                $liste->setDateMariage(null);
            } else {
                $liste->setDateMariage($editForm["dateMariage"]->getData());
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($liste);
            $em->flush();
            $this->addFlash('success', 'Votre liste a été modifier avec succés');

            return $this->render(
                '@MyWeddingProduit/ListCouple/editListeProfil.html.twig',
                [
                    'form' => $editForm->createView(),
                ]
            );
        }

        return $this->render(
            '@MyWeddingProduit/ListCouple/editListeProfil.html.twig',
            [
                'form' => $editForm->createView(),
            ]
        );
    }

    public
    function changerPhotoCouvertureAction(
        Request $request
    ) {
        $cover2 = new Picture();
        $cover = $request->files->get('cover');

        if ($cover != null) {
            $cover2->setFile($cover);
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $liste = $em->getRepository('MyWeddingProduitBundle:ListCouple')->findOneBy(
                array('user' => $user->getId())
            );
            $liste->setPhotoDeCouverture($cover2);
            $em->persist($liste);
            $em->flush();

            return $this->redirect($request->headers->get('referer'));

        }

        return $this->render('@MyWeddingProduit/ListCouple/editCoverPicture.html.twig');
    }

    public
    function changerPhotoProfilAction(
        Request $request
    ) {
        $cover2 = new Picture();
        $cover = $request->files->get('profil-pic');

        if ($cover != null) {
            $cover2->setFile($cover);
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $liste = $em->getRepository('MyWeddingProduitBundle:ListCouple')->findOneBy(
                array('user' => $user->getId())
            );
            $liste->setPhotoDeProfil($cover2);
            $em->persist($liste);
            $em->flush();

            return $this->redirect($request->headers->get('referer'));

        }

        return $this->render('@MyWeddingProduit/ListCouple/editprofilPicture.html.twig');
    }


    /**
     * Creates a form to create a ListCouple entity.
     *
     * @param ListCouple $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createCreateForm(
        ListCouple $entity
    ) {
        $form = $this->createForm(
            new ListCoupleType(),
            $entity,
            array(
                'action' => $this->generateUrl('listcouple_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }


    /**
     * Displays a form to create a new ListCouple entity.
     *
     */
    public
    function newAction()
    {
        $entity = new ListCouple();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'MyWeddingProduitBundle:ListCouple:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }


    /**
     * Finds and displays a ListCouple entity.
     *
     */
    public
    function showAction(
        $id
    ) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:ListCouple')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }
        $products = $em->getRepository('MyWeddingProduitBundle:DetailsProduit')->findBy(array('listeCouple_id' => $entity->getId()));
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'MyWeddingProduitBundle:ListCouple:show.html.twig',
            array(
                'entity' => $entity,
                'collectes' => $entity->getCollecte(),
                'products' => $products,
                'commandes' => $entity->getCommandList(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    public
    function chercherAction(
        Request $request
    ) {

        $request = $this->get('request');

        $name = ($request->get('find'));


        $listeCouple = new ListCouple();


        $em = $this->getDoctrine()->getManager();
        $listeCouple = $em->getRepository('MyWeddingProduitBundle:ListCouple')
            ->findOneBy(array('name' => $name));


        if (!$listeCouple) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }


        $produits = $listeCouple->getProducts();


        return $this->render(
            '@MyWeddingProduit/ListCouple/showListe.html.twig',
            array(
                'collectes' => $listeCouple->getCollecte(),
                'liste' => $listeCouple,
                'produits' => $produits,
                'name' => $listeCouple->getName(),

            )
        );


    }


    public
    function listsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:ListCouple')->findAll();


        return $this->render(
            'MyWeddingProduitBundle:ListCouple:allLists.html.twig',
            array(
                'entities' => $entities,
            )
        );
    }


    /**
     * Displays a form to edit an existing ListCouple entity.
     *
     */
    public
    function editAction(
        $id
    ) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:ListCouple')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'MyWeddingProduitBundle:ListCouple:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Creates a form to edit a ListCouple entity.
     *
     * @param ListCouple $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createEditForm(
        ListCouple $entity
    ) {
        $form = $this->createForm(
            new ListCoupleType(),
            $entity,
            array(
                'action' => $this->generateUrl('listcouple_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing ListCouple entity.
     *
     */
    public
    function updateAction(
        Request $request,
        $id
    ) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:ListCouple')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('listcouple_edit', array('id' => $id)));
        }

        return $this->render(
            'MyWeddingProduitBundle:ListCouple:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a ListCouple entity.
     *
     */
    public
    function deleteAction(
        Request $request,
        $id
    ) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MyWeddingProduitBundle:ListCouple')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ListCouple entity.');
        }

        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('listcouple'));
    }


    /**
     * Creates a form to delete a ListCouple entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private
    function createDeleteForm(
        $id
    ) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('listcouple_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
}
