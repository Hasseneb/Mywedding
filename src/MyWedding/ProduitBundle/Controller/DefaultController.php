<?php

namespace MyWedding\ProduitBundle\Controller;

use Buzz\Message\Request;
use MyWedding\ProduitBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $recommended = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:Category')->findRecommendedOrderedByName();
        if ($this->getRequest()->getSession()->has('products')){
            $session = $this->getRequest()->getSession()->get('products');
        }
        return $this->render('MyWeddingProduitBundle:Default:index.html.twig',[
            'recommended' => $recommended
        ]);
    }
    public function signupAction()
    {
        $recommended = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:Category')->findRecommendedOrderedByName();
        if ($this->getRequest()->getSession()->has('products')){
            $session = $this->getRequest()->getSession()->get('products');
        }
        $this->getRequest()->getSession()->set('valider',true);
        return $this->render('MyWeddingProduitBundle:Default:indexSignup.html.twig',[
            'recommended' => $recommended
        ]);
    }

    public function aproposAction()
    {
        return $this->render('MyWeddingProduitBundle:Default:Apropos.html.twig');
    }

    public function smsAction()
    {
        $sid = "AC6f499278486a407b2cb328480b2d4535"; // Your Account SID from www.twilio.com/console
        $token = "d837b9e0e24c4769842955aa260c2859"; // Your Auth Token from www.twilio.com/console
        $client = new \Twilio\Rest\Client($sid, $token);
        $message = $client->messages->create(
            '+21654256111', // Text this number
            array(
                'from' => '+21654256111', // From a valid Twilio number
                'body' => 'MyyWeeding!'
            )
        );
        print $message->sid;
    }
}