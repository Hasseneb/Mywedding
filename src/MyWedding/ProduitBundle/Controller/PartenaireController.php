<?php

namespace MyWedding\ProduitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use MyWedding\ProduitBundle\Entity\Picture;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProduitBundle\Entity\Partenaire;
use MyWedding\ProduitBundle\Form\PartenaireType;

/**
 * Partenaire controller.
 *
 */
class PartenaireController extends Controller
{

    /**
     * Lists all Partenaire entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:Partenaire')->findAll();

        return $this->render('MyWeddingProduitBundle:Partenaire:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function allPartenaireAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:Partenaire')->findAll();

        return $this->render('MyWeddingProduitBundle:Partenaire:allPartenaire.html.twig', array(
            'entities' => $entities,
        ));
    }


    /**
     * Creates a new Partenaire entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Partenaire();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $picture = new Picture();
            $em = $this->getDoctrine()->getManager();
            $picture->upload();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('partenaire_show', array('id' => $entity->getId())));
        }

        return $this->render('MyWeddingProduitBundle:Partenaire:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Partenaire entity.
     *
     * @param Partenaire $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Partenaire $entity)
    {
        $form = $this->createForm(new PartenaireType(), $entity, array(
            'action' => $this->generateUrl('partenaire_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Partenaire entity.
     *
     */
    public function newAction()
    {
        $entity = new Partenaire();
        $form   = $this->createCreateForm($entity);

        return $this->render('MyWeddingProduitBundle:Partenaire:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Partenaire entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Partenaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partenaire entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:Partenaire:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Partenaire entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Partenaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partenaire entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:Partenaire:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Partenaire entity.
    *
    * @param Partenaire $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Partenaire $entity)
    {
        $form = $this->createForm(new PartenaireType(), $entity, array(
            'action' => $this->generateUrl('partenaire_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Partenaire entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Partenaire')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Partenaire entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('partenaire_edit', array('id' => $id)));
        }

        return $this->render('MyWeddingProduitBundle:Partenaire:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Partenaire entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingProduitBundle:Partenaire')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Partenaire entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('partenaire'));
    }

    /**
     * Creates a form to delete a Partenaire entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('partenaire_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
