<?php

namespace MyWedding\ProduitBundle\Controller;

use MyWedding\ProduitBundle\Entity\Category;
use MyWedding\ProduitBundle\Entity\Couleur;
use MyWedding\ProduitBundle\Entity\DetailsProduit;
use MyWedding\ProduitBundle\Entity\ListCouple;
use MyWedding\ProduitBundle\Entity\liste_produit_assoc;
use MyWedding\ProduitBundle\Entity\Picture;
use SalesBundle\Entity\Cart;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\DBAL\Query\QueryBuilder;
use MyWedding\ProduitBundle\Entity\Product;
use MyWedding\ProduitBundle\Form\ProductType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Product controller.
 *
 */
class ProductController extends Controller
{

    /**
     * Lists all Product entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:Product')->findAll();


        return $this->render(
            'MyWeddingProduitBundle:Product:index.html.twig',
            array(
                'entities' => $entities,
            )
        );
    }


    public function productsAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $produits = $em->getRepository('MyWeddingProduitBundle:Product')->findBy(array('enabled' => true));
        $marques = $em->getRepository('MyWeddingProduitBundle:Mark')->findAll();
        $enabledCardProducts = $em->getRepository('MyWeddingProduitBundle:Product')->findBy(
            array('enabled' => true, 'enableCard' => true)
        );
        $entities = $this->get('knp_paginator')->paginate(
            $produits, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            1/*limit per page*/
        );

        return $this->render(
            'MyWeddingProduitBundle:Product:allProduct.html.twig',
            array(
                'entities' => $entities,
                'enabledCardProducts' => $enabledCardProducts,
                'marques' => $marques,
            )
        );
    }

    public function productColorsByCategoryAction($category)
    {
        $couleurs = [];
        foreach ($category->getProducts() as $product ){
            foreach ($product->getCouleur() as $couleur){
                if (!array_search($couleur,$couleurs)){
                    array_push($couleurs,$couleur);
                }
            }

        }
        return $this->render('@MyWeddingProduit/Product/couleurs.html.twig',[
            'couleurs' => $couleurs
        ]);
    }

    public function productsContributionAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:Product')->findAll();


        return $this->render(
            'MyWeddingProduitBundle:Product:productsToContribute.html.twig',
            array(
                'entities' => $entities,
            )
        );

    }

    public function caracteristProduitAction($id)
    {


        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);

        return $this->render(
            'MyWeddingProduitBundle:Product:caracterisProduit.html.twig',
            array(
                'product' => $product,
            )
        );


    }

    public function chercherProduitAction(Request $request)
    {

        $request = $this->get('request');

        $name = ($request->get('chercher'));
        $repositoryManager = $this->container->get('fos_elastica.manager.orm');
        $repository = $repositoryManager->getRepository('MyWeddingProduitBundle:Product');
        $produits = $repository->find($name);


        if (!$produits) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }


        return $this->render(
            'MyWeddingProduitBundle:Product:allProduct.html.twig',
            array(
                'entities' => $produits,

            )
        );


    }


    public function getSousCategorieAction($category){
        $categorie = $this->getDoctrine()->getRepository("MyWeddingProduitBundle:Category")->findBy(array('name' => $category));
        $sousCategorie = $this->getDoctrine()->getRepository("MyWeddingProduitBundle:SousCategory")->findBy(array('category'=>$categorie));
        return $this->render('@MyWeddingProduit/Product/getSousCategorie.html.twig',[
            'sousCategorie' => $sousCategorie,
        ]);
    }

    /**
     * Creates a new Product entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Product();
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $picture = new Picture();
            $em = $this->getDoctrine()->getManager();
            $picture->upload();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('product_show', array('id' => $entity->getId())));
        }

        return $this->render(
            'MyWeddingProduitBundle:Product:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );


    }

    /**
     * Creates a form to create a Product entity.
     *
     * @param Product $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Product $entity)
    {
        $form = $this->createForm(
            new ProductType(),
            $entity,
            array(
                'action' => $this->generateUrl('product_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Product entity.
     *
     */
    public function newAction()
    {

        $entity = new Product();

        $form = $this->createCreateForm($entity);

        return $this->render(
            'MyWeddingProduitBundle:Product:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    public function addProductListAction($id)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $id_user = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('MyWeddingProduitBundle:ListCouple')->findOneBy(array('user' => $id_user));
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);
        if (!($list->getProducts()->contains($product))) {
            $list->addProduct($product);
            $detailsProduit = new DetailsProduit();
            $detailsProduit->setProduitId($product->getId());
            $detailsProduit->setListeCoupleId($list->getId());
            $detailsProduit->setQuantiteVoulu(1);
            $detailsProduit->setQuantiteAchete(0);
            $detailsProduit->setPrixPiece($product->getPrix());
        } else {
            $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
                ->findOneBy(array('listeCouple_id' => $list->getId(), 'produit_id' => $product->getId()));
            $detailsProduit->setQuantiteVoulu($detailsProduit->getQuantiteVoulu() + 1);
        }
        $em->persist($detailsProduit);
        $em->persist($list);
        $em->flush();
        $referer = $this->getRequest()->headers->get('referer');
        return $this->redirect($referer);



    }


    public function addProductPanierAction($id)
    {

        $user = $this->get('security.context')->getToken()->getUser();
        $id_user = $user->getId(); // 3
        $em = $this->getDoctrine()->getManager();
        $pan = $em->getRepository('MyWeddingProduitBundle:Basket')->findOneBy(array('user' => $user));
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);
        $pan->addProduct($product);
        $em->persist($pan);
        $em->flush();

        return $this->redirect($this->generateUrl('product_all'));

        return $this->render('MyWeddingProduitBundle:Product:allProduct.html.twig');


    }

    public function deleteProductPanierAction($id)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $id_user = $user->getId(); // 3
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(array('user' => $user,'status' => Cart::STATUS_PROCESSING));
        $em = $this->getDoctrine()->getManager();
        $item = $em->getRepository('SalesBundle:CartItem')->find($id);
        $cart->removeItem($item);
        $em->persist($cart);
        $em->flush();
        $referer = $this->getRequest()->headers->get('referer');

        return $this->redirect($referer);

    }


    public function deleteProductListAction(Request $request, $id)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $list = $em->getRepository('MyWeddingProduitBundle:ListCouple')->findOneBy(array('user' => $user));
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);
        $detailsProduit = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:DetailsProduit')
            ->findOneBy(array('listeCouple_id' => $list->getId(), 'produit_id' => $product->getId()));
        $em->remove($detailsProduit);
        $list->removeProduct($product);
        $em->persist($list);
        $em->flush();


        return $this->redirect($request->headers->get('referer'));

    }


    public function listeProduitParCategorieAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('MyWeddingProduitBundle:Category')->find($id);
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('MyWeddingProduitBundle:Product')
            ->findBy(array('category' => $category, 'enabled' => true));
        $enabledCardProducts = $em->getRepository('MyWeddingProduitBundle:Product')->findBy(
            array('enabled' => true, 'enableCard' => true, 'category' => $category)
        );
        $entities = $this->get('knp_paginator')->paginate(
            $produits, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );


        return $this->render(
            'MyWeddingProduitBundle:Product:showSelonCategory.html.twig',
            array('produits' => $entities, 'category' => $category, 'enabledCardProducts' => $enabledCardProducts)
        );
    }


    public function listeProduitParSouCategorieAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $souCategory = $em->getRepository('MyWeddingProduitBundle:SousCategory')
            ->find($id);
        $em = $this->getDoctrine()->getManager();
        $produits = $em->getRepository('MyWeddingProduitBundle:Product')
            ->findBy(array('souCategory' => $souCategory));
        $enabledCardProducts = $em->getRepository('MyWeddingProduitBundle:Product')->findBy(
            array('enabled' => true, 'enableCard' => true, 'souCategory' => $souCategory)
        );
        $entities = $this->get('knp_paginator')->paginate(
            $produits, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        return $this->render(
            'MyWeddingProduitBundle:Product:showSelonSouCategory.html.twig',
            array('produits' => $entities, 'souCategory' => $souCategory, 'enabledCardProducts' => $enabledCardProducts)
        );

    }

    public function filtreAjaxCategorieAction(Request $request,$category)
    {

        $sousCategories = $request->query->get('sousCategorie');
        $prix = $request->query->get('prix');
        $marques = $request->query->get('marques');
        $notes = $request->query->get('rate');
        $couleurs = $request->query->get('couleurs');
        $em = $this->getDoctrine()->getManager();
        $sousCategories = $em->getRepository('MyWeddingProduitBundle:SousCategory')->findBy(array('name' => $sousCategories));
        $marques = $em->getRepository('MyWeddingProduitBundle:Mark')->findBy(array('title'=>$marques));
        $category = $em->getRepository('MyWeddingProduitBundle:Category')->find($category);
        $couleurs = $em->getRepository('MyWeddingProduitBundle:Couleur')->findBy(array('nom' => $couleurs));
        $products = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:Product')->findProductsByCategory($category,$sousCategories,$prix,$marques,$notes,$couleurs);
        $productsByPrice = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:Product')->findProductsByPrice($category,$prix);
        $products = array_intersect($products,$productsByPrice);




        return $this->render('@MyWeddingProduit/Product/produitFiltrer.html.twig',[
            'produits' => $products
        ]);
    }
    public function filtreAjaxSousCategorieAction(Request $request,$category)
    {

        $sousCategories = $request->query->get('sousCategorie');
        $prix = $request->query->get('prix');
        $marques = $request->query->get('marques');
        $notes = $request->query->get('rate');
        $couleurs = $request->query->get('couleurs');
        $em = $this->getDoctrine()->getManager();
        $sousCategories = $em->getRepository('MyWeddingProduitBundle:SousCategory')->findBy(array('name' => $sousCategories));
        $marques = $em->getRepository('MyWeddingProduitBundle:Mark')->findBy(array('title'=>$marques));
        $category = $em->getRepository('MyWeddingProduitBundle:SousCategory')->find($category);
        $couleurs = $em->getRepository('MyWeddingProduitBundle:Couleur')->findBy(array('nom' => $couleurs));
        $products = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:Product')->findProductsBySousCategory($category,$sousCategories,$prix,$marques,$notes,$couleurs);
        $productsByPrice = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:Product')->findProductsByPrice($category,$prix);
        $products = array_intersect($products,$productsByPrice);




        return $this->render('@MyWeddingProduit/Product/produitFiltrer.html.twig',[
            'produits' => $products
        ]);
    }


    /**
     *
     */
    public function donnerAvisAction(Request $request, $id)
    {
        $avis = $request->request->get('rating');

        if ($avis != null) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository('MyWeddingProduitBundle:Product')->findOneBy(array('id' => $id));

            if ($product->getAvis() == 0) {
                $product->setAvis(($product->getAvis() + $avis));
            } else {
                $product->setAvis(($product->getAvis() + $avis) / 2);
            }
            $product->setNbAvis(($product->getNbAvis() + 1));
            $em->persist($product);
            $em->flush();
            $this->addFlash('success', 'Votre avis a été bien enregistrer');

            return $this->render(
                '@MyWeddingProduit/Product/avis.html.twig',
                [
                    'id' => $id,
                ]
            );

        }

        return $this->render(
            '@MyWeddingProduit/Product/avis.html.twig',
            [
                'id' => $id,
            ]
        );
    }


    /**
     * Finds and displays a Product entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'MyWeddingProduitBundle:Product:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }


    /**
     * Displays a form to edit an existing Product entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);


        return $this->render(
            'MyWeddingProduitBundle:Product:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Creates a form to edit a Product entity.
     *
     * @param Product $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Product $entity)
    {
        $form = $this->createForm(
            new ProductType(),
            $entity,
            array(
                'action' => $this->generateUrl('product_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Product entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $data = $editForm["pictures"]->getData();
            foreach ($data as $picture){
                $picture->preUpload();
                $entity->setPicture($picture);
                $em->persist($picture);
            }
            $em->flush();
            $this->addFlash('success', 'Le produit a été modifier avec succés');

            return $this->redirect($this->generateUrl('product_edit', array('id' => $id)));
        }

        return $this->render(
            'MyWeddingProduitBundle:Product:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a Product entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }


        $em->remove($entity);
        $em->flush();


        return $this->redirect($this->generateUrl('product'));
    }

    /**
     * Creates a form to delete a Product entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('product_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }


}
