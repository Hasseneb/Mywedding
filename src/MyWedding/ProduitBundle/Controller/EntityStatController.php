<?php

namespace MyWedding\ProduitBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProduitBundle\Entity\EntityStat;
use MyWedding\ProduitBundle\Form\EntityStatType;

/**
 * EntityStat controller.
 *
 */
class EntityStatController extends Controller
{

    /**
     * Lists all EntityStat entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

      $entities = $em->getRepository('MyWeddingProduitBundle:EntityStat')->findAll();



        return $this->render('MyWeddingProduitBundle:EntityStat:index.html.twig', array(
            'entities' => $entities,
        ));
    }


    public function listerTemoignageAction()
    {
        $em = $this->getDoctrine()->getManager();


        $entities=$em->getRepository('MyWeddingProduitBundle:EntityStat')->findBy(array('type' => 'Témoignage'));


        return $this->render('MyWeddingProduitBundle:EntityStat:temoignage.html.twig', array(
            'entities' => $entities,
        ));
    }

    public function listerQuiSommeNousAction()
    {
        $em = $this->getDoctrine()->getManager();


        $entities=$em->getRepository('MyWeddingProduitBundle:EntityStat')->findBy(array('type' => 'QuiSommeNous'));


        return $this->render('MyWeddingProduitBundle:EntityStat:quiSommeN.html.twig', array(
            'entities' => $entities,
        ));
    }






    /**
     * Creates a new EntityStat entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new EntityStat();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('entitystat_show', array('id' => $entity->getId())));
        }

        return $this->render('MyWeddingProduitBundle:EntityStat:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EntityStat entity.
     *
     * @param EntityStat $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EntityStat $entity)
    {
        $form = $this->createForm(new EntityStatType(), $entity, array(
            'action' => $this->generateUrl('entitystat_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EntityStat entity.
     *
     */
    public function newAction()
    {
        $entity = new EntityStat();
        $form   = $this->createCreateForm($entity);

        return $this->render('MyWeddingProduitBundle:EntityStat:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EntityStat entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:EntityStat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EntityStat entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:EntityStat:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EntityStat entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:EntityStat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EntityStat entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:EntityStat:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a EntityStat entity.
    *
    * @param EntityStat $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(EntityStat $entity)
    {
        $form = $this->createForm(new EntityStatType(), $entity, array(
            'action' => $this->generateUrl('entitystat_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EntityStat entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:EntityStat')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EntityStat entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('entitystat_edit', array('id' => $id)));
        }

        return $this->render('MyWeddingProduitBundle:EntityStat:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a EntityStat entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingProduitBundle:EntityStat')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EntityStat entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('entitystat'));
    }

    /**
     * Creates a form to delete a EntityStat entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('entitystat_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
