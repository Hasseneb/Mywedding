<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 20/07/2017
 * Time: 12:38
 */

namespace MyWedding\ProduitBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DetailsProduitController extends Controller
{
    public function getProductNameAction($id){
        $product = $this->getDoctrine()->getRepository('MyWeddingProduitBundle:Product')->find($id);
        return new Response($product->getName());

    }

}