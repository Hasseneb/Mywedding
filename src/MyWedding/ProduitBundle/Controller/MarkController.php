<?php

namespace MyWedding\ProduitBundle\Controller;

use MyWedding\ProduitBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProduitBundle\Entity\Mark;
use MyWedding\ProduitBundle\Form\MarkType;

/**
 * Mark controller.
 *
 */
class MarkController extends Controller
{

    /**
     * Lists all Mark entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingProduitBundle:Mark')->findAll();

        return $this->render('MyWeddingProduitBundle:Mark:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Mark entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Mark();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('mark_show', array('id' => $entity->getId())));
        }

        return $this->render('MyWeddingProduitBundle:Mark:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }


    public function listerMarksAction($category) {
        $em= $this->getDoctrine()->getManager();
        $category = $em->getRepository('MyWeddingProduitBundle:Category')->find($category);
        $products = $category->getProducts();
        $marques = array();
        foreach ($products as $product){
            if ($product->getMarque() != null){
                array_push($marques,$product->getMarque());
            }
        }


        return $this->render('MyWeddingProduitBundle:Mark:showMark.html.twig',
            array('marks' =>     array_unique($marques),
            )
        );
    }

    /**
     * Creates a form to create a Mark entity.
     *
     * @param Mark $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Mark $entity)
    {
        $form = $this->createForm(new MarkType(), $entity, array(
            'action' => $this->generateUrl('mark_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Mark entity.
     *
     */
    public function newAction()
    {
        $entity = new Mark();
        $form   = $this->createCreateForm($entity);

        return $this->render('MyWeddingProduitBundle:Mark:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Mark entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Mark')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mark entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:Mark:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Mark entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Mark')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mark entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingProduitBundle:Mark:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Mark entity.
    *
    * @param Mark $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Mark $entity)
    {
        $form = $this->createForm(new MarkType(), $entity, array(
            'action' => $this->generateUrl('mark_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Mark entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:Mark')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mark entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('mark_edit', array('id' => $id)));
        }

        return $this->render('MyWeddingProduitBundle:Mark:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Mark entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingProduitBundle:Mark')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Mark entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('mark'));
    }

    /**
     * Creates a form to delete a Mark entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('mark_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
