<?php

namespace MyWedding\ProduitBundle\Controller;

use MyWedding\CollecteBundle\Entity\Notification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\ProduitBundle\Entity\CommandList;
use MyWedding\ProduitBundle\Form\CommandListType;

/**
 * CommandList controller.
 *
 */
class CommandListController extends Controller
{

    /**
     * Lists all CommandList entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::commandList));
        $nombre_notification = count($notifications);
        $entities = $em->getRepository('MyWeddingProduitBundle:CommandList')->findAllOrdered();

        return $this->render(
            'MyWeddingProduitBundle:CommandList:index.html.twig',
            array(
                'entities' => $entities,
                'notification' => $nombre_notification
            )
        );
    }

    /**
     * Creates a new CommandList entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new CommandList();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('commandlist_show', array('id' => $entity->getId())));
        }

        return $this->render(
            'MyWeddingProduitBundle:CommandList:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Creates a form to create a CommandList entity.
     *
     * @param CommandList $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(CommandList $entity)
    {
        $form = $this->createForm(
            new CommandListType(),
            $entity,
            array(
                'action' => $this->generateUrl('commandlist_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new CommandList entity.
     *
     */
    public function newAction()
    {
        $entity = new CommandList();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'MyWeddingProduitBundle:CommandList:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Finds and displays a CommandList entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:CommandList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CommandList entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'MyWeddingProduitBundle:CommandList:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing CommandList entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:CommandList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CommandList entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'MyWeddingProduitBundle:CommandList:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Creates a form to edit a CommandList entity.
     *
     * @param CommandList $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(CommandList $entity)
    {
        $form = $this->createForm(
            new CommandListType(),
            $entity,
            array(
                'action' => $this->generateUrl('commandlist_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing CommandList entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingProduitBundle:CommandList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find CommandList entity.');
        }
        $is_received_old_value = $entity->getIsRecieved();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $detailsProduit = $em->getRepository('MyWeddingProduitBundle:DetailsProduit')
                ->findOneBy(
                    array(
                        'listeCouple_id' => $entity->getList()->getId(),
                        'produit_id' => $entity->getProduct(),
                    )
                );

            if ($is_received_old_value == false && $editForm['isRecieved']->getData() == true) {
                $quantity = $editForm['quantity']->getData();
                $quantiteRestante = $detailsProduit->getQuantiteRestante();
                if (($quantiteRestante - $quantity) >= 0) {
                    $detailsProduit->setQuantiteAchete($detailsProduit->getQuantiteAchete() + $quantity);
                } else {
                    $this->addFlash(
                        'CommandListe_erreur',
                        'La quantité dépasse le quantité voulue ('.$quantiteRestante.') du produit , '
                    );

                    return $this->redirect($this->generateUrl('commandlist_edit', array('id' => $id)));
                }

            } elseif ($is_received_old_value == true && $editForm['isRecieved']->getData() == false) {
                $quantity = $editForm['quantity']->getData();
                $quantiteAchete = $detailsProduit->getQuantiteAchete();
                $quantiteRestante = $detailsProduit->getQuantiteRestante();
                if (($quantiteRestante - $quantity) >= 0) {
                    $detailsProduit->setQuantiteAchete($quantiteAchete - $quantity);
                } else {
                    $this->addFlash(
                        'CommandListe_erreur',
                        'La quantité dépasse le quantité voulue ('.$quantiteRestante.') du produit , '
                    );

                    return $this->redirect($this->generateUrl('commandlist_edit', array('id' => $id)));
                }

            } elseif ($is_received_old_value == true && $editForm['isRecieved']->getData() == true) {
                $quantity = $editForm['quantity']->getData();
                $quantiteAchete = $detailsProduit->getQuantiteAchete();
                $quantiteRestante = $detailsProduit->getQuantiteRestante();
                if (($quantiteRestante - $quantity) >= 0) {
                    $detailsProduit->setQuantiteAchete($quantiteAchete - $quantity);
                } else {
                    $this->addFlash(
                        'CommandListe_erreur',
                        'La quantité dépasse le quantité voulue ('.$quantiteRestante.') du produit , '
                    );

                    return $this->redirect($this->generateUrl('commandlist_edit', array('id' => $id)));
                }
            }

            $em->persist($entity);
            $em->persist($detailsProduit);
            $em->flush();


            return $this->redirect($this->generateUrl('commandlist_edit', array('id' => $id)));
        }

        return $this->render(
            'MyWeddingProduitBundle:CommandList:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a CommandList entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MyWeddingProduitBundle:CommandList')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Command list entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('commandlist'));
    }

    /**
     * Creates a form to delete a CommandList entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commandlist_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    public function newCommandListAction(Request $request, $ListId, $ProductId)
    {
        $quantity = ($request->get('quantity'));
        $nom = ($request->get('nom'));
        $prenom = ($request->get('prenom'));
        $tel = ($request->get('tel'));
        $message = ($request->get('message'));
        $paymentId = ($request->get('payment'));
        $em = $this->getDoctrine()->getManager();
        $paymentMethod = $em->getRepository('MyWeddingProduitBundle:Payment')->find($paymentId);
        $liste = $em->getRepository('MyWeddingProduitBundle:ListCouple')->find($ListId);
        $produit = $em->getRepository('MyWeddingProduitBundle:Product')->find($ProductId);

        $entity = new CommandList();
        if ($user = $this->getUser()) {
            $entity->setUser($user);
        } else {
            $entity->setBuyerLastname($nom);
            $entity->setBuyerFirstname($prenom);
            $entity->setBuyerNumber($tel);
        }
        $entity->setProduct($produit);
        $entity->setList($liste);
        $entity->setPaymentMethod($paymentMethod);
        $entity->setMessage($message);
        $entity->setQuantity($quantity);

        $em = $this->getDoctrine()->getManager();
       $notification = new Notification(Notification::commandList);
        $em->persist($notification);
        $em->persist($entity);
        $em->flush();
        $referer = $request->headers->get('referer');

        return $this->redirect($referer);
    }
}
