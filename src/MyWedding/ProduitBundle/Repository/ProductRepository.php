<?php
namespace MyWedding\ProduitBundle\Repository;

use Doctrine\Common\Collections\Criteria;

/**
 * Created by PhpStorm.
 * User: micro
 * Date: 02/06/2017
 * Time: 10:04
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository
{

    public function findColorsByCategory($category)
    {
        return $this->createQueryBuilder('color')
            ->andWhere('color.enabled = :enabled')
            ->andWhere('color.category = :category')
            ->setParameter('category', $category)
            ->setParameter('enabled', true)
            ->distinct(true)
            ->getQuery()
            ->execute();

    }

    public function findProductsByCategory($category, $souscategory, $prix, $marques, $notes, $couleurs)
    {
        $query = $this->createQueryBuilder('products')
            ->where('products.category = :category')
            ->setParameter('category', $category)
            ->andWhere('products.enabled = :enabled')
            ->setParameter('enabled', true);
        if ($souscategory != null) {
            $query->andWhere('products.souCategory IN (:sc)')->setParameter('sc', array_values($souscategory));
        }
        if ($marques != null) {
            $query->andWhere('products.marque IN (:marques)')->setParameter('marques', array_values($marques));
        }
        if ($notes != null) {
            $query->andWhere('products.avis IN (:notes)')->setParameter('notes', array_values($notes));
        }
        if ($couleurs != null) {
            $query->andWhere('products.couleur IN (:couleurs)')->setParameter('couleurs', array_values($couleurs));
        }


        return $query->getQuery()->execute();


    }
    public function findProductsBySousCategory($category, $souscategory, $prix, $marques, $notes, $couleurs)
    {
        $query = $this->createQueryBuilder('products')
            ->where('products.souCategory = :category')
            ->setParameter('category', $category)
            ->andWhere('products.enabled = :enabled')
            ->setParameter('enabled', true);
        if ($souscategory != null) {
            $query->andWhere('products.souCategory IN (:sc)')->setParameter('sc', array_values($souscategory));
        }
        if ($marques != null) {
            $query->andWhere('products.marque IN (:marques)')->setParameter('marques', array_values($marques));
        }
        if ($notes != null) {
            $query->andWhere('products.avis IN (:notes)')->setParameter('notes', array_values($notes));
        }
        if ($couleurs != null) {
            $query->andWhere('products.couleur IN (:couleurs)')->setParameter('couleurs', array_values($couleurs));
        }


        return $query->getQuery()->execute();


    }

    public function findProductsByPrice($category, $prix)
    {
        $query2 = $this->createQueryBuilder('productByPrice')
            ->andWhere('productByPrice.enabled = :enabled')
            ->setParameter('enabled', true);

        if ($prix != null) {
            $i = 0;
            foreach ($prix as $price) {
                if ($i == 0) {
                    switch ($price) {
                        case 1 :
                            $query2->andWhere('productByPrice.prix < :prix')->setParameter('prix', 50);
                            break;
                        case 2 :
                            $query2->andWhere(
                                'productByPrice.prix >= :prix1 AND productByPrice.prix < :prix2'
                            )->setParameter(
                                'prix1',
                                50
                            )->setParameter('prix2', 150);
                            break;
                        case 3 :
                            $query2->andWhere(
                                'productByPrice.prix >= :prix3 AND productByPrice.prix < :prix4'
                            )->setParameter(
                                'prix3',
                                150
                            )->setParameter('prix4', 300);
                            break;
                        case 4 :
                            $query2->andWhere('productByPrice.prix >= :prix5')->setParameter('prix5', 300);
                            break;
                    }
                    $i++;
                }
                else{
                    switch ($price) {
                        case 1 :
                            $query2->orWhere('productByPrice.prix < :prix')->setParameter('prix', 50);
                            break;
                        case 2 :
                            $query2->orWhere(
                                'productByPrice.prix >= :prix1 AND productByPrice.prix < :prix2'
                            )->setParameter(
                                'prix1',
                                50
                            )->setParameter('prix2', 150);
                            break;
                        case 3 :
                            $query2->orWhere(
                                'productByPrice.prix >= :prix3 AND productByPrice.prix < :prix4'
                            )->setParameter(
                                'prix3',
                                150
                            )->setParameter('prix4', 300);
                            break;
                        case 4 :
                            $query2->orWhere('productByPrice.prix >= :prix5')->setParameter('prix5', 300);
                            break;
                    }
                    $i++;
                }


            }


        }

        return $query2->getQuery()->execute();
    }

}
