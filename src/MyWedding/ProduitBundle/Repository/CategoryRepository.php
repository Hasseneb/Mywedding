<?php
namespace MyWedding\ProduitBundle\Repository;
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 02/06/2017
 * Time: 10:04
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{

    public function findRecommendedOrderedByName()
    {
        return $this->createQueryBuilder('category')
            ->andWhere('category.flash = :flash')
            ->andWhere('category.enabled = :enabled')
            ->setParameter('flash',true)
            ->setParameter('enabled',true)
            ->orderBy('category.createdAt','DESC')
            ->setMaxResults(6)
            ->getQuery()
            ->execute();

    }


}