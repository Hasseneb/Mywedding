<?php
namespace MyWedding\ProduitBundle\Repository;
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 02/06/2017
 * Time: 10:04
 */
class CommandlistRepository extends \Doctrine\ORM\EntityRepository
{

    public function findAllOrdered()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM MyWeddingProduitBundle:CommandList p ORDER BY p.createdAt DESC '
            )->getResult();

    }


}