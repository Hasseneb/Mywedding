<?php
namespace  MyWedding\ProduitBundle\Repository;

use Doctrine\ORM\EntityRepository;


class CommandeRepository extends EntityRepository
{


    public function cmdParMois()
    {
        $query =$this->getEntityManager()->createQuery("select count(c) , month(c.date) as month_x from MyWeddingProduitBundle:Commande c group by month_x ");

        return $query->getResult();
    }
}