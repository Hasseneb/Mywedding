<?php

namespace MyWedding\ProduitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EntityStatType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'choice', array(
            'choices'   => array(
                'Témoignage'   => 'Témoignage',
                'QuiSommeNous' => 'QuiSommeNous',
            )

        ))
            ->add('title')
            ->add('description')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\ProduitBundle\Entity\EntityStat'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mywedding_produitbundle_entitystat';
    }
}
