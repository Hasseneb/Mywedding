<?php

namespace MyWedding\ProduitBundle\Form;

use MyWedding\ProduitBundle\Entity\Picture;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('prix')
            ->add('stock')
            ->add('enableCard')
            ->add('enabled')
            ->add('marque')
            ->add('description')
            ->add(
                'couleur',
                'entity',
                array(
                    'class' => 'MyWedding\ProduitBundle\Entity\Couleur',
                    'multiple' => true,
                    'expanded' => true,
                    'choice_attr' => function ($val, $key, $index) {
                        return ['style' => 'margin: 0 10px 0 10px;'];
                    },
                )
            )
            ->add('fraisLivraison')
            ->add('category')
            ->add('souCategory')
            ->add('reference')
            ->add('poids')
            ->add('dimensions')
            ->add('garantie')
            ->add(
                'pictures',
                'collection',
                array(
                    'type' => new PictureType(),
                    'allow_add' => true,
                    'by_reference' => false,
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'MyWedding\ProduitBundle\Entity\Product',
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mywedding_produitbundle_product';
    }
}
