<?php

namespace MyWedding\ProduitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ListCoupleProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add("prenom")
            ->add("nom")
            ->add("typeUser",'choice', array(
                'choices'  => array(
                    'Marié' => 'Marié',
                    'Mariée' => 'Mariée',
                ),
                'label_attr' => array('class' => 'field-label'),
                'expanded' => true,
                'multiple'=> false,
                'choices_as_values' => true,
            ))
            ->add('numTel')
            ->add("prenomPartenaire")
            ->add("nomPartenaire")
            ->add("typePartenaire",'choice', array(
                'choices'  => array(
                    'Marié' => 'Marié',
                    'Mariée' => 'Mariée',
                ),
                'label_attr' => array('class' => 'field-label'),
                'expanded' => true,
                'multiple'=> false,
                'choices_as_values' => true,
            ))
            ->add("emailPartenaire")
            ->add("gerelistePartenaire",'choice', array(
                'choices'  => array(
                    'Absolument' => true,
                    'Pas maintenant' => false,
                ),
                'expanded' => false,
                'multiple'=> false,
                'choices_as_values' => true,
            ))
            ->add('ville')
            ->add('adress')
            ->add('codePostal')
            ->add("lien")
            ->add("dateMariage",'date',array(
                'widget' => 'single_text',
            ))
            ->add("couverture", new PictureType(),array('required' => false))
            ->add("profil", new PictureType(),array('required' => false))
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
            $resolver->setDefaults(array(
//               'data_class' => 'MyWedding\ProduitBundle\Entity\ListCouple',
                'cascade_validation' => true,
            ));
    }


    public function getName()
    {
        return 'my_wedding_produit_bundle_list_couple_profile_type';
    }

}
