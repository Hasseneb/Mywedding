<?php

namespace MyWedding\ProduitBundle\Form;

use Symfony\Component\Form\AbstractType;
use MyWedding\ProduitBundle\Entity\Picture;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('picture',new PictureType())
            ->add('flash','choice', array(
                'choices'  => array(
                    'Oui' => true,
                    'Non' => false,),
                'choices_as_values' => true,
            ))
            ->add('enabled','choice', array(
                'choices'  => array(
                    'Oui' => true,
                    'Non' => false,),
                'choices_as_values' => true,
            ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\ProduitBundle\Entity\Category'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mywedding_produitbundle_category';
    }
}
