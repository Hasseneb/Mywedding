<?php

namespace MyWedding\ProduitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CommandListType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('buyerFirstname')
            ->add('buyerLastname')
            ->add('buyerNumber')
            ->add('message')
            ->add('quantity')
            ->add('isRecieved','choice',array(
                'choices' => array(
                    'Recu' => true,
                    'Non Recu' => false
                ),
                'choices_as_values' => true
            ))
            ->add('user')
            ->add('product')
            ->add('paymentMethod')
            ->add('list')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\ProduitBundle\Entity\CommandList'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mywedding_produitbundle_commandlist';
    }
}
