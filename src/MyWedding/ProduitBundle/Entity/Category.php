<?php

namespace MyWedding\ProduitBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MyWedding\ProduitBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255,nullable=true)
     */
    private $description;
    /**
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $flash;
    /**
     * @ORM\Column(type="boolean")
     */
    private  $enabled;

    /**
     * @return mixed
     */
    public function getFlash()
    {
        return $this->flash;
    }

    /**
     * @param mixed $flash
     */
    public function setFlash($flash)
    {
        $this->flash = $flash;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }


    /**
     * @ORM\OneToOne(targetEntity="Picture",cascade={"all"})
     * @ORM\JoinColumn(name="picture_id", referencedColumnName="id")
     */
    private $picture;


    /**
     * @ORM\OneToMany(targetEntity="MyWedding\ProduitBundle\Entity\SousCategory",mappedBy="category",cascade={"remove"} )
     */
    private  $souCategories;

    /**
     * @ORM\OneToMany(targetEntity="Product",mappedBy="category")
     */
    private $products;


    public function __toString()
    {
        return (string) $this->getName();
    }



    public function __construct()
    {
        $this->createdAt = new \DateTime('NOW');
        $this->souCategories = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }




    /**
     * @return mixed
     */
    public function getGallery()
    {
        return $this->gallery;
    }

    /**
     * @param mixed $gallery
     */
    public function setGallery($gallery)
    {
        $this->gallery = $gallery;
    }





    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }



    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return mixed
     */
    public function getSouCategories()
    {
        return $this->souCategories;
    }

    /**
     * @param mixed $souCategories
     */
    public function setSouCategories($souCategories)
    {
        $this->souCategories = $souCategories;
    }





    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Add souCategories
     *
     * @param \MyWedding\ProduitBundle\Entity\SousCategory $souCategories
     * @return Category
     */
    public function addSouCategory(\MyWedding\ProduitBundle\Entity\SousCategory $souCategories)
    {

        $this->souCategories[] = $souCategories;

        return $this;
    }

    /**
     * Remove souCategories
     *
     * @param \MyWedding\ProduitBundle\Entity\SousCategory $souCategories
     */
    public function removeSouCategory(\MyWedding\ProduitBundle\Entity\SousCategory $souCategories)
    {
        $this->souCategories->removeElement($souCategories);
    }

    /**
     * Add products
     *
     * @param \MyWedding\ProduitBundle\Entity\Product $products
     * @return Category
     */
    public function addProduct(\MyWedding\ProduitBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \MyWedding\ProduitBundle\Entity\Product $products
     */
    public function removeProduct(\MyWedding\ProduitBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }



    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

}
