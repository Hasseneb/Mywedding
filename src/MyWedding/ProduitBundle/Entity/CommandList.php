<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 11/07/2017
 * Time: 10:13
 */

namespace MyWedding\ProduitBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="MyWedding\ProduitBundle\Repository\CommandlistRepository")
 * @ORM\Table(name="command_list")
 */
class CommandList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\UserBundle\Entity\User",inversedBy="commandList")
     */
    private $user;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $buyerFirstname;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $buyerLastname;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $buyerNumber;
    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\ProduitBundle\Entity\Product",inversedBy="commandList")
     */
    private $product;
    /**
     * @ORM\Column(type="string")
     */
    private $message;
    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isRecieved=false;
    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\ProduitBundle\Entity\Payment",inversedBy="commandList")
     */
    private $paymentMethod;
    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\ProduitBundle\Entity\ListCouple",inversedBy="commandList")
     */
    private $list;
    /**
     * Constructor
     */
    public function __construct()
    {
       $this->createdAt = new \DateTime('now');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set buyerFirstname
     *
     * @param string $buyerFirstname
     *
     * @return CommandList
     */
    public function setBuyerFirstname($buyerFirstname)
    {
        $this->buyerFirstname = $buyerFirstname;

        return $this;
    }

    /**
     * Get buyerFirstname
     *
     * @return string
     */
    public function getBuyerFirstname()
    {
        return $this->buyerFirstname;
    }

    /**
     * Set buyerLastname
     *
     * @param string $buyerLastname
     *
     * @return CommandList
     */
    public function setBuyerLastname($buyerLastname)
    {
        $this->buyerLastname = $buyerLastname;

        return $this;
    }

    /**
     * Get buyerLastname
     *
     * @return string
     */
    public function getBuyerLastname()
    {
        return $this->buyerLastname;
    }

    /**
     * Set buyerNumber
     *
     * @param string $buyerNumber
     *
     * @return CommandList
     */
    public function setBuyerNumber($buyerNumber)
    {
        $this->buyerNumber = $buyerNumber;

        return $this;
    }

    /**
     * Get buyerNumber
     *
     * @return string
     */
    public function getBuyerNumber()
    {
        return $this->buyerNumber;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format(" j F, Y, g:i a");
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Set product
     *
     * @param string $product
     *
     * @return CommandList
     */
    public function setProduct($product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Add user
     *
     * @param \MyWedding\UserBundle\Entity\User $user
     *
     * @return CommandList
     */
    public function addUser(\MyWedding\UserBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \MyWedding\UserBundle\Entity\User $user
     */
    public function removeUser(\MyWedding\UserBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set list
     *
     * @param \MyWedding\ProduitBundle\Entity\ListCouple $list
     *
     * @return CommandList
     */
    public function setList(\MyWedding\ProduitBundle\Entity\ListCouple $list = null)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return \MyWedding\ProduitBundle\Entity\ListCouple
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set paymentMethod
     *
     * @param \MyWedding\ProduitBundle\Entity\Payment $paymentMethod
     *
     * @return CommandList
     */
    public function setPaymentMethod(\MyWedding\ProduitBundle\Entity\Payment $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \MyWedding\ProduitBundle\Entity\Payment
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return CommandList
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return CommandList
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set isRecieved
     *
     * @param boolean $isRecieved
     *
     * @return CommandList
     */
    public function setIsRecieved($isRecieved)
    {
        $this->isRecieved = $isRecieved;

        return $this;
    }

    /**
     * Get isRecieved
     *
     * @return boolean
     */
    public function getIsRecieved()
    {
        return $this->isRecieved;
    }

    /**
     * Set user
     *
     * @param \MyWedding\UserBundle\Entity\User $user
     *
     * @return CommandList
     */
    public function setUser(\MyWedding\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
