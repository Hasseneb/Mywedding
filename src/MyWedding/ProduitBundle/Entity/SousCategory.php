<?php

namespace MyWedding\ProduitBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * sousCategory
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SousCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255,nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToOne(targetEntity="Picture",cascade={"all"})
     * @ORM\JoinColumn(name="picture_id", referencedColumnName="id")
     */
    private $picture;

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }



    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\ProduitBundle\Entity\Category", inversedBy="souCategories")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=false)
     */
    private  $category;




    /**
     * @ORM\OneToMany(targetEntity="MyWedding\ProduitBundle\Entity\Product",mappedBy="souCategory")
     */
    private $products;




    public function __toString()
    {
        return (string) $this->getName();
    }



    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }



    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pictures = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Set category
     *
     * @param \MyWedding\ProduitBundle\Entity\Category $category
     * @return SousCategory
     */
    public function setCategory(\MyWedding\ProduitBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \MyWedding\ProduitBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add products
     *
     * @param \MyWedding\ProduitBundle\Entity\Product $products
     * @return SousCategory
     */
    public function addProduct(\MyWedding\ProduitBundle\Entity\Product $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \MyWedding\ProduitBundle\Entity\Product $products
     */
    public function removeProduct(\MyWedding\ProduitBundle\Entity\Product $products)
    {
        $this->products->removeElement($products);
    }


}
