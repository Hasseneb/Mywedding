<?php

namespace MyWedding\ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use MyWedding\ProduitBundle\Form\PictureType;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ListCouple
 *
 * @ORM\Table()
 * @ORM\Entity
 * @UniqueEntity(fields="name",message="Nom de la liste existe déja")
 */
class ListCouple
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="Name", type="string", length=255,unique=true)
     */

    private $name;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isEnabled = true;

    /**
     * @return mixed
     */
    public function getIsEnabled()
    {
        return $this->isEnabled;
    }

    /**
     * @param mixed $isEnabled
     */
    public function setIsEnabled($isEnabled)
    {
        $this->isEnabled = $isEnabled;
    }

    /**
     * @ORM\ManyToMany(targetEntity="MyWedding\CollecteBundle\Entity\Collecte",inversedBy="list")
     */
    private $collecte;


    /**
     * @ORM\OneToMany(targetEntity="MyWedding\ProduitBundle\Entity\CommandList",mappedBy="list")
     */
    private $commandList;
    /**
     * @ORM\OneToOne(targetEntity="Picture",cascade={"all"})
     * @ORM\JoinColumn(name="photoDeProfil_id", referencedColumnName="id")
     */
    private $photoDeProfil;
    /**
     * @ORM\OneToOne(targetEntity="Picture",cascade={"all"})
     * @ORM\JoinColumn(name="photoDeCouverture_id", referencedColumnName="id")
     */
    private $photoDeCouverture;

    /**
     * @ORM\ManyToMany(targetEntity="MyWedding\ProduitBundle\Entity\Product", inversedBy="listeCouple" )
     * @ORM\JoinTable(name="lists_products" )
     */
    private $products;


    /**
     * @ORM\OneToOne(targetEntity="MyWedding\UserBundle\Entity\User",inversedBy="listeCouple",cascade={"all"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(type="date",nullable=true)
     */
    private $dateMariage;

    /**
     * @return mixed
     */
    public function getPhotoDeProfil()
    {
        return $this->photoDeProfil;
    }

    /**
     * @param mixed $photoDeProfil
     */
    public function setPhotoDeProfil(Picture $photoDeProfil)
    {
        $this->photoDeProfil = $photoDeProfil;
    }

    /**
     * @return mixed
     */
    public function getPhotoDeCouverture()
    {
        return $this->photoDeCouverture;
    }

    /**
     * @param mixed $photoDeCouverture
     */
    public function setPhotoDeCouverture(Picture $photoDeCouverture)
    {
        $this->photoDeCouverture = $photoDeCouverture;
    }


    /**
     * @return mixed
     */
    public function getDateMariage()
    {
        if ($this->dateMariage != null) {
//            return $this->dateMariage->format('d/m/Y');
            return $this->dateMariage;
        } else {
            return '';
        }

    }

    /**
     * @param mixed $dateMariage
     */
    public function setDateMariage($dateMariage)
    {
        $this->dateMariage = $dateMariage;
    }


    public function __toString()
    {
        return (string)$this->getName();
    }


    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return ListCouple
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ListCouple
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->contributions = new ArrayCollection();
    }

    public function addProduct(Product $product)
    {

        $this->products[] = $product;

        return $this;
    }

    public function removeProduct(Product $product)
    {

        $this->products->removeElement($product);
    }


    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }



    /**
     * Set collecte
     *
     * @param \MyWedding\CollecteBundle\Entity\Collecte $collecte
     *
     * @return ListCouple
     */
    public function setCollecte(\MyWedding\CollecteBundle\Entity\Collecte $collecte = null)
    {
        $this->collecte = $collecte;

        return $this;
    }

    /**
     * Get collecte
     *
     * @return \MyWedding\CollecteBundle\Entity\Collecte
     */
    public function getCollecte()
    {
        return $this->collecte;
    }

    /**
     * Add collecte
     *
     * @param \MyWedding\CollecteBundle\Entity\Collecte $collecte
     *
     * @return ListCouple
     */
    public function addCollecte(\MyWedding\CollecteBundle\Entity\Collecte $collecte)
    {
        $this->collecte[] = $collecte;

        return $this;
    }

    /**
     * Remove collecte
     *
     * @param \MyWedding\CollecteBundle\Entity\Collecte $collecte
     */
    public function removeCollecte(\MyWedding\CollecteBundle\Entity\Collecte $collecte)
    {
        $this->collecte->removeElement($collecte);
    }

    /**
     * Add commandList
     *
     * @param \MyWedding\ProduitBundle\Entity\CommandList $commandList
     *
     * @return ListCouple
     */
    public function addCommandList(\MyWedding\ProduitBundle\Entity\CommandList $commandList)
    {
        $this->commandList[] = $commandList;

        return $this;
    }

    /**
     * Remove commandList
     *
     * @param \MyWedding\ProduitBundle\Entity\CommandList $commandList
     */
    public function removeCommandList(\MyWedding\ProduitBundle\Entity\CommandList $commandList)
    {
        $this->commandList->removeElement($commandList);
    }

    /**
     * Get commandList
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandList()
    {
        return $this->commandList;
    }
}
