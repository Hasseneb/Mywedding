<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 30/06/2017
 * Time: 12:53
 */

namespace MyWedding\ProduitBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="couleur")
 */
class Couleur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $nom;
    /**
     * @ORM\Column(type="string")
     */
    private $hexCode;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Couleur
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set hexCode
     *
     * @param string $hexCode
     * @return Couleur
     */
    public function setHexCode($hexCode)
    {
        $this->hexCode = $hexCode;

        return $this;
    }

    /**
     * Get hexCode
     *
     * @return string 
     */
    public function getHexCode()
    {
        return $this->hexCode;
    }

    function __toString()
    {
        return $this->nom;
    }


}
