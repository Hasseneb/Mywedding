<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 14/06/2017
 * Time: 12:48
 */

namespace MyWedding\ProduitBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="details_produit")
 */
class DetailsProduit
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="integer")
     */
    private $listeCouple_id;
    /**
     * @ORM\Column(type="integer")
     */
    private $produit_id;
    /**
     * @ORM\Column(type="integer")
     */
    private $quantiteVoulu;
    /**
     * @ORM\Column(type="integer")
     */
    private $quantiteAchete = 0;

    /**
     * @ORM\Column(type="float")
     */
    private $prixPiece ;

    /**
     * @return mixed
     */
    public function getPrixPiece()
    {
        return $this->prixPiece;
    }

    /**
     * @param mixed $prixPiece
     */
    public function setPrixPiece($prixPiece)
    {
        $this->prixPiece = $prixPiece;
    }




    /**
     * @return mixed
     */
    public function getQuantiteRestante(){
            return $this->getQuantiteVoulu()-$this->getQuantiteAchete();
    }

    /**
     * @return mixed
     */
    public function getListeCoupleId()
    {
        return $this->listeCouple_id;
    }

    /**
     * @param mixed $listeCouple_id
     */
    public function setListeCoupleId($listeCouple_id)
    {
        $this->listeCouple_id = $listeCouple_id;
    }

    /**
     * @return mixed
     */
    public function getProduitId()
    {
        return $this->produit_id;
    }

    /**
     * @param mixed $produit_id
     */
    public function setProduitId($produit_id)
    {
        $this->produit_id = $produit_id;
    }

    /**
     * @return mixed
     */
    public function getQuantiteVoulu()
    {
        return $this->quantiteVoulu;
    }

    /**
     * @param mixed $quantiteVoulu
     */
    public function setQuantiteVoulu($quantiteVoulu)
    {
        $this->quantiteVoulu = $quantiteVoulu;
    }

    /**
     * @return mixed
     */
    public function getQuantiteAchete()
    {
        return $this->quantiteAchete;
    }

    /**
     * @param mixed $quantiteAchete
     */
    public function setQuantiteAchete($quantiteAchete)
    {
        $this->quantiteAchete = $quantiteAchete;
    }
}