<?php

namespace MyWedding\ProduitBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MyWedding\ProduitBundle\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="float",nullable=true)
     */
    private $fraisLivraison;


    /**
     * @ORM\ManyToMany(targetEntity="MyWedding\ProduitBundle\Entity\Couleur")
     */
    private $couleur;

    /**
     * @return mixed
     */
    public function getFraisLivraison()
    {
        return $this->fraisLivraison;
    }

    /**
     * @param mixed $fraisLivraison
     */
    public function setFraisLivraison($fraisLivraison)
    {
        $this->fraisLivraison = $fraisLivraison;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     */
    private $name;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $reference;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $poids;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $couleurs;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $dimensions;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $garantie;
    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var float
     *
     * @ORM\Column(name="nbrAdd", type="integer",nullable=true)
     */
    private $nbrAdd;

    /**
     * @var float
     *
     * @ORM\Column(name="stock", type="integer")
     */
    private $stock;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="date", nullable=true)
     */
    private $createdDate;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="UpdatedDate", type="date", nullable=true)
     */
    private $UpdatedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;


    /**
     * @var boolean
     *
     * @ORM\Column(name="enableCard", type="boolean", nullable=true)
     */
    private $enableCard;

    /**
     * @ORM\OneToMany(targetEntity="MyWedding\ProduitBundle\Entity\CommandList",mappedBy="product")
     */
    private $commandList;
    /**
     * @ORM\ManyToOne(targetEntity="Category",inversedBy="products")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;


    /**
     * @ORM\ManyToMany(targetEntity="ListCouple", mappedBy="products")
     */
    private $listsCouple;





    /**
     * @ORM\ManyToOne(targetEntity="SousCategory",inversedBy="products")
     * @ORM\JoinColumn(name="SousCategory_id", referencedColumnName="id")
     */
    private $souCategory;


    /**
     * @ORM\ManyToOne(targetEntity="Promotion",inversedBy="products")
     * @ORM\JoinColumn(name="promotion_id", referencedColumnName="id")
     */
    private $promotion;


    /**
     * @ORM\ManyToOne(targetEntity="Mark",inversedBy="products")
     * @ORM\JoinColumn(name="marque_id", referencedColumnName="id")
     */
    private $marque;

    /**
     * @ORM\OneToMany(targetEntity="MyWedding\CollecteBundle\Entity\Contribution",mappedBy="produit")
     */
    private $contribution;


//    /**
//     * @ORM\OnetoOne(targetEntity="MyWedding\CollecteBundle\Entity\Collecte", inversedBy="produit",cascade={"all"})
//     */
//    private $collecte;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $avis = 0;

    /**
     * @return mixed
     */
    public function getAvis()
    {
        return $this->avis;
    }

    /**
     * @param mixed $avis
     */
    public function setAvis($avis)
    {
        $this->avis = $avis;
    }

    /**
     * @return mixed
     */
    public function getNbAvis()
    {
        return $this->nbAvis;
    }

    /**
     * @param mixed $nbAvis
     */
    public function setNbAvis($nbAvis)
    {
        $this->nbAvis = $nbAvis;
    }

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $nbAvis;

    /**
     * @ORM\ManyToMany(targetEntity="MyWedding\ProduitBundle\Entity\ListCouple",inversedBy="products")
     */
    private $listeCouple;

    /**
     * @return mixed
     */
    public function getListeCouple()
    {
        return $this->listeCouple;
    }

    /**
     * @param mixed $listeCouple
     */
    public function setListeCouple(ListCouple $listeCouple)
    {
        $this->listeCouple = $listeCouple;
    }


    /**
     * @ORM\OneToMany(targetEntity="Picture",mappedBy="produit",cascade={"all"})
     *
     */
    private $pictures;


    public function __toString()
    {

        return (string)$this->getName();
    }

    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return mixed
     */
    public function getListsCouple()
    {
        return $this->listsCouple;
    }

    /**
     * @param mixed $listsCouple
     */
    public function setListsCouple($listsCouple)
    {
        $this->listsCouple = $listsCouple;
    }






    /**
     * @return mixed
     */
    public function getPromotion()
    {
        return $this->promotion;
    }

    /**
     * @param mixed $promotion
     */
    public function setPromotion($promotion)
    {
        $this->promotion = $promotion;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * @param mixed $poids
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;
    }

    /**
     * @return mixed
     */
    public function getCouleurs()
    {
        return $this->couleurs;
    }

    /**
     * @param mixed $couleurs
     */
    public function setCouleurs($couleurs)
    {
        $this->couleurs = $couleurs;
    }

    /**
     * @return mixed
     */
    public function getDimensions()
    {
        return $this->dimensions;
    }

    /**
     * @param mixed $dimensions
     */
    public function setDimensions($dimensions)
    {
        $this->dimensions = $dimensions;
    }

    /**
     * @return mixed
     */
    public function getGarantie()
    {
        return $this->garantie;
    }

    /**
     * @param mixed $garantie
     */
    public function setGarantie($garantie)
    {
        $this->garantie = $garantie;
    }


    /**
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->UpdatedDate;
    }

    /**
     * @param \DateTime $UpdatedDate
     */
    public function setUpdatedDate($UpdatedDate)
    {
        $this->UpdatedDate = $UpdatedDate;
    }

    /**
     * @return boolean
     */
    public function isEnableCard()
    {
        return $this->enableCard;
    }

    /**
     * @param boolean $enableCard
     */
    public function setEnableCard($enableCard)
    {
        $this->enableCard = $enableCard;
    }


    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }


    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set prix
     *
     * @param float $prix
     * @return Product
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Product
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return float
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param float $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getMarque()
    {
        return $this->marque;
    }

    /**
     * @param mixed $marque
     */
    public function setMarque($marque)
    {
        $this->marque = $marque;
    }


    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Set category
     *
     * @param \MyWedding\ProduitBundle\Entity\Category $category
     * @return Product
     */
    public function setCategory(\MyWedding\ProduitBundle\Entity\Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \MyWedding\ProduitBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set souCategory
     *
     * @param \MyWedding\ProduitBundle\Entity\SousCategory $souCategory
     * @return Product
     */
    public function setSouCategory(\MyWedding\ProduitBundle\Entity\SousCategory $souCategory = null)
    {
        $this->souCategory = $souCategory;

        return $this;
    }

    /**
     * Get souCategory
     *
     * @return \MyWedding\ProduitBundle\Entity\SousCategory
     */
    public function getSouCategory()
    {
        return $this->souCategory;
    }

    /**
     * Get enableCard
     *
     * @return boolean
     */
    public function getEnableCard()
    {
        return $this->enableCard;
    }

    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->pictures = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add listsCouple
     *
     * @param \MyWedding\ProduitBundle\Entity\ListCouple $listsCouple
     * @return Product
     */
    public function addListsCouple(\MyWedding\ProduitBundle\Entity\ListCouple $listsCouple)
    {
        $this->listsCouple[] = $listsCouple;

        return $this;
    }

    /**
     * Remove listsCouple
     *
     * @param \MyWedding\ProduitBundle\Entity\ListCouple $listsCouple
     */
    public function removeListsCouple(\MyWedding\ProduitBundle\Entity\ListCouple $listsCouple)
    {
        $this->listsCouple->removeElement($listsCouple);
    }





    /**
     * Set nbrAdd
     *
     * @param integer $nbrAdd
     * @return Product
     */
    public function setNbrAdd($nbrAdd)
    {
        $this->nbrAdd = $nbrAdd;

        return $this;
    }

    /**
     * Get nbrAdd
     *
     * @return integer
     */
    public function getNbrAdd()
    {
        return $this->nbrAdd;
    }


    /**
     * Add pictures
     *
     * @param \MyWedding\ProduitBundle\Entity\Picture $pictures
     * @return Product
     */
    public function addPicture(\MyWedding\ProduitBundle\Entity\Picture $pictures)
    {
        $pictures->setProduit($this);
        $this->pictures[] = $pictures;

        return $this;
    }

    /**
     * Remove pictures
     *
     * @param \MyWedding\ProduitBundle\Entity\Picture $pictures
     */
    public function removePicture(\MyWedding\ProduitBundle\Entity\Picture $pictures)
    {
        $pictures->setProduit(null);

        $this->pictures->removeElement($pictures);
    }

    /**
     * Get pictures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPictures()
    {
        return $this->pictures;
    }


    /**
     * Add contribution
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     * @return Product
     */
    public function addContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $this->contribution[] = $contribution;

        return $this;
    }

    /**
     * Remove contribution
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     */
    public function removeContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $this->contribution->removeElement($contribution);
    }

    /**
     * Get contribution
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Set collecte
     *
     * @param \MyWedding\CollecteBundle\Entity\Collecte $collecte
     * @return Product
     */
    public function setCollecte(\MyWedding\CollecteBundle\Entity\Collecte $collecte = null)
    {
        $this->collecte = $collecte;

        return $this;
    }

    /**
     * Get collecte
     *
     * @return \MyWedding\CollecteBundle\Entity\Collecte
     */
    public function getCollecte()
    {
        return $this->collecte;
    }

    /**
     * Add couleur
     *
     * @param \MyWedding\ProduitBundle\Entity\Couleur $couleur
     * @return Product
     */
    public function addCouleur(\MyWedding\ProduitBundle\Entity\Couleur $couleur)
    {
        $this->couleur[] = $couleur;

        return $this;
    }

    /**
     * Remove couleur
     *
     * @param \MyWedding\ProduitBundle\Entity\Couleur $couleur
     */
    public function removeCouleur(\MyWedding\ProduitBundle\Entity\Couleur $couleur)
    {
        $this->couleur->removeElement($couleur);
    }

    /**
     * Get couleur
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Add listeCouple
     *
     * @param \MyWedding\ProduitBundle\Entity\ListCouple $listeCouple
     * @return Product
     */
    public function addListeCouple(\MyWedding\ProduitBundle\Entity\ListCouple $listeCouple)
    {
        $this->listeCouple[] = $listeCouple;

        return $this;
    }

    /**
     * Remove listeCouple
     *
     * @param \MyWedding\ProduitBundle\Entity\ListCouple $listeCouple
     */
    public function removeListeCouple(\MyWedding\ProduitBundle\Entity\ListCouple $listeCouple)
    {
        $this->listeCouple->removeElement($listeCouple);
    }

    /**
     * Add commandList
     *
     * @param \MyWedding\ProduitBundle\Entity\CommandList $commandList
     *
     * @return Product
     */
    public function addCommandList(\MyWedding\ProduitBundle\Entity\CommandList $commandList)
    {
        $this->commandList[] = $commandList;

        return $this;
    }

    /**
     * Remove commandList
     *
     * @param \MyWedding\ProduitBundle\Entity\CommandList $commandList
     */
    public function removeCommandList(\MyWedding\ProduitBundle\Entity\CommandList $commandList)
    {
        $this->commandList->removeElement($commandList);
    }

    /**
     * Get commandList
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandList()
    {
        return $this->commandList;
    }
}
