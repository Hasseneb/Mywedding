<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 05/07/2017
 * Time: 09:59
 */

namespace MyWedding\ProduitBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="payment")
 */
class Payment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;
    /**
     * @ORM\Column(type="string")
     */
    private $message;
    /**
     * @ORM\Column(type="string")
     */
    private $coordonnes;
    /**
     * @ORM\OneToMany(targetEntity="MyWedding\CollecteBundle\Entity\Contribution",mappedBy="paymentMethod")
     */
    private $contribution;
    /**
     * @ORM\OneToMany(targetEntity="SalesBundle\Entity\Cart",mappedBy="paymentMethod")
     */
    private $cart;
    /**
     * @ORM\OneToMany(targetEntity="MyWedding\ProduitBundle\Entity\CommandList",mappedBy="paymentMethod")
     */
    private $commandList;

    function __toString()
    {
        return $this->getName();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Payment
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return Payment
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set coordonnes
     *
     * @param string $coordonnes
     *
     * @return Payment
     */
    public function setCoordonnes($coordonnes)
    {
        $this->coordonnes = $coordonnes;

        return $this;
    }

    /**
     * Get coordonnes
     *
     * @return string
     */
    public function getCoordonnes()
    {
        return $this->coordonnes;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contribution = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add contribution
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     *
     * @return Payment
     */
    public function addContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $this->contribution[] = $contribution;

        return $this;
    }

    /**
     * Remove contribution
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     */
    public function removeContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $this->contribution->removeElement($contribution);
    }

    /**
     * Get contribution
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Add commandList
     *
     * @param \MyWedding\ProduitBundle\Entity\CommandList $commandList
     *
     * @return Payment
     */
    public function addCommandList(\MyWedding\ProduitBundle\Entity\CommandList $commandList)
    {
        $this->commandList[] = $commandList;

        return $this;
    }

    /**
     * Remove commandList
     *
     * @param \MyWedding\ProduitBundle\Entity\CommandList $commandList
     */
    public function removeCommandList(\MyWedding\ProduitBundle\Entity\CommandList $commandList)
    {
        $this->commandList->removeElement($commandList);
    }

    /**
     * Get commandList
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCommandList()
    {
        return $this->commandList;
    }

    /**
     * Add cart
     *
     * @param \SalesBundle\Entity\Cart $cart
     *
     * @return Payment
     */
    public function addCart(\SalesBundle\Entity\Cart $cart)
    {
        $this->cart[] = $cart;

        return $this;
    }

    /**
     * Remove cart
     *
     * @param \SalesBundle\Entity\Cart $cart
     */
    public function removeCart(\SalesBundle\Entity\Cart $cart)
    {
        $this->cart->removeElement($cart);
    }

    /**
     * Get cart
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCart()
    {
        return $this->cart;
    }
}
