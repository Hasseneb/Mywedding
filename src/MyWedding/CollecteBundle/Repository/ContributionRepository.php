<?php
namespace MyWedding\CollecteBundle\Repository;
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 02/06/2017
 * Time: 10:04
 */
class ContributionRepository extends \Doctrine\ORM\EntityRepository
{

    public function findAllOrdered()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT p FROM MyWeddingCollecteBundle:Contribution p ORDER BY p.createdAt DESC '
            )->getResult();

    }


}