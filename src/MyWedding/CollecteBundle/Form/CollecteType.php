<?php

namespace MyWedding\CollecteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CollecteType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nameProduit')
            ->add('list')
            ->add('sommeAtteinte')
            ->add('produit')
            ->add('user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\CollecteBundle\Entity\Collecte'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mywedding_collectebundle_collecte';
    }
}
