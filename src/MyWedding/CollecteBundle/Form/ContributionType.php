<?php

namespace MyWedding\CollecteBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContributionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('somme')
            ->add('donnneur')
            ->add('collecte')
            ->add('nomDonneur')
            ->add('prenomDonneur')
            ->add('tel')
            ->add('message')
            ->add('paymentMethod')
            ->add('IsReceived','choice',array(
                'choices' => array(
                    'Recu' => true,
                    'Non Recu' => false
                ),
                'choices_as_values' => true
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MyWedding\CollecteBundle\Entity\Contribution'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'mywedding_collectebundle_contribution';
    }
}
