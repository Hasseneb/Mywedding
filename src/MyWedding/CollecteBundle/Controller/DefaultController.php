<?php

namespace MyWedding\CollecteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MyWeddingCollecteBundle:Default:index.html.twig', array('name' => $name));
    }
}
