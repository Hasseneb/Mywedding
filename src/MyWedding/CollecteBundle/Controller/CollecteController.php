<?php

namespace MyWedding\CollecteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\CollecteBundle\Entity\Collecte;
use MyWedding\CollecteBundle\Form\CollecteType;

/**
 * Collecte controller.
 *
 */
class CollecteController extends Controller
{

    /**
     * Lists all Collecte entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingCollecteBundle:Collecte')->findAll();

        return $this->render('MyWeddingCollecteBundle:Collecte:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new Collecte entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Collecte();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('collecte_show', array('id' => $entity->getId())));
        }

        return $this->render('MyWeddingCollecteBundle:Collecte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Collecte entity.
     *
     * @param Collecte $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Collecte $entity)
    {
        $form = $this->createForm(new CollecteType(), $entity, array(
            'action' => $this->generateUrl('collecte_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Collecte entity.
     *
     */
    public function newAction()
    {
        $entity = new Collecte();
        $form   = $this->createCreateForm($entity);

        return $this->render('MyWeddingCollecteBundle:Collecte:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    public function newCollecteAction($id)
    {

        $user= $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getManager();
        $list=$em->getRepository('MyWeddingProduitBundle:ListCouple')->findOneBy(array('user' => $user));
        $id_list= $list->getId();
        $em = $this->getDoctrine()->getManager();
        $product=$em->getRepository('MyWeddingProduitBundle:Product')->find($id);

        $entity = new Collecte();
        $entity->addProduit($product);
        $name=$product->getName();
        $entity->setListId($id_list);
        $entity->setNameProduit($name);
        $entity->setPrixProduit($product->getPrix());
        $entity->setUser($user);
        $list->addCollecte($entity);

        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->persist($list);
        $em->flush();

        $referer = $this->getRequest()->headers->get('referer');

        return $this->redirect($referer);
    }


    /**
     * Finds and displays a Collecte entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingCollecteBundle:Collecte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Collecte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingCollecteBundle:Collecte:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Collecte entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingCollecteBundle:Collecte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Collecte entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingCollecteBundle:Collecte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Collecte entity.
    *
    * @param Collecte $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Collecte $entity)
    {
        $form = $this->createForm(new CollecteType(), $entity, array(
            'action' => $this->generateUrl('collecte_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Collecte entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingCollecteBundle:Collecte')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Collecte entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('collecte_edit', array('id' => $id)));
        }

        return $this->render('MyWeddingCollecteBundle:Collecte:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Collecte entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingCollecteBundle:Collecte')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Collecte entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('collecte'));
    }

    /**
     * Creates a form to delete a Collecte entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('collecte_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
