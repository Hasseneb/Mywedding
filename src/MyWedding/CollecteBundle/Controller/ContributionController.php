<?php

namespace MyWedding\CollecteBundle\Controller;

use MyWedding\CollecteBundle\Entity\Notification;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use MyWedding\CollecteBundle\Entity\Contribution;
use MyWedding\CollecteBundle\Form\ContributionType;

/**
 * Contribution controller.
 *
 */
class ContributionController extends Controller
{

    /**
     * Lists all Contribution entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingCollecteBundle:Contribution')->findAllOrdered();
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::contribution));
        $nombre_notification = count($notifications);
        return $this->render('MyWeddingCollecteBundle:Contribution:index.html.twig', array(
            'entities' => $entities,
            'notification' => $nombre_notification
        ));
    }
    /**
     * Creates a new Contribution entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new Contribution();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('contribution_show', array('id' => $entity->getId())));
        }

        return $this->render('MyWeddingCollecteBundle:Contribution:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }



    public function newContributionAction(Request $request,$id)
    {
        $somme  = ($request->get('price'));
        $nom  = ($request->get('nom'));
        $prenom  = ($request->get('prenom'));
        $tel  = ($request->get('tel'));
        $message  = ($request->get('message'));
        $paymentId  = ($request->get('payment'));
        $em = $this->getDoctrine()->getManager();
        $collecte =$em->getRepository('MyWeddingCollecteBundle:Collecte')->find($id);
        $paymentMethod =$em->getRepository('MyWeddingProduitBundle:Payment')->find($paymentId);

        $entity = new Contribution();
        if ($user = $this->getUser()){
            $entity->setDonnneur($user);
        }else{
            $entity->setNomDonneur($nom);
            $entity->setPrenomDonneur($prenom);
            $entity->setTel($tel);

        }
        $entity->setPaymentMethod($paymentMethod);
        $entity->setMessage($message);
        $entity->setSomme($somme);
        $collecte->addContribution($entity);

        $em = $this->getDoctrine()->getManager();
        $notification = new Notification(Notification::contribution);
        $em->persist($notification);
        $em->persist($entity);
        $em->persist($collecte);
        $em->flush();
        $referer = $request->headers->get('referer');
        return $this->redirect($referer);
    }

    /**
     * Creates a form to create a Contribution entity.
     *
     * @param Contribution $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Contribution $entity)
    {
        $form = $this->createForm(new ContributionType(), $entity, array(
            'action' => $this->generateUrl('contribution_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Contribution entity.
     *
     */
    public function newAction()
    {
        $entity = new Contribution();
        $form   = $this->createCreateForm($entity);

        return $this->render('MyWeddingCollecteBundle:Contribution:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Contribution entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingCollecteBundle:Contribution')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contribution entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingCollecteBundle:Contribution:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Contribution entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingCollecteBundle:Contribution')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contribution entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('MyWeddingCollecteBundle:Contribution:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Contribution entity.
    *
    * @param Contribution $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Contribution $entity)
    {
        $form = $this->createForm(new ContributionType(), $entity, array(
            'action' => $this->generateUrl('contribution_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Contribution entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingCollecteBundle:Contribution')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contribution entity.');
        }
        $is_received_old_value =$entity->getIsReceived();
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            if ( $is_received_old_value == false && $editForm['IsReceived']->getData() == true ){
                $sommeDonne = $editForm['somme']->getData();
                $sommeAtteinte = $entity->getCollecte()->getSommeAtteinte();
                if (($sommeAtteinte + $sommeDonne) <= $entity->getCollecte()->getPrixProduit()) {
                    $entity->getCollecte()->setSommeAtteinte($sommeAtteinte+$sommeDonne);
                }else{
                    $this->addFlash('Contribution_erreur','La somme dépasse le prix du produit');
                    return $this->redirect($this->generateUrl('contribution_edit', array('id' => $id)));
                }

            }elseif ($is_received_old_value == true && $editForm['IsReceived']->getData() == false ){
                $sommeDonne = $editForm['somme']->getData();
                $sommeAtteinte = $entity->getCollecte()->getSommeAtteinte();
                $entity->getCollecte()->setSommeAtteinte($sommeAtteinte-$sommeDonne);

            }
            $em->persist($entity);
            $em->persist($entity->getCollecte());
            $em->flush();

            return $this->redirect($this->generateUrl('contribution_edit', array('id' => $id)));
        }

        return $this->render('MyWeddingCollecteBundle:Contribution:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a Contribution entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);


            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingCollecteBundle:Contribution')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Contribution entity.');
            }

            $em->remove($entity);
            $em->flush();


        return $this->redirect($this->generateUrl('contribution'));
    }

    /**
     * Creates a form to delete a Contribution entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contribution_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
