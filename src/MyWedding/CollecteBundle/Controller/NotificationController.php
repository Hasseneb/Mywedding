<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 06/07/2017
 * Time: 13:16
 */

namespace MyWedding\CollecteBundle\Controller;


use MyWedding\CollecteBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{
    public function countNotificationContributionAction()
    {
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::contribution));
        $nb = count($notifications);

        return new Response($nb);
    }
    public function countNotificationCommandeAction()
    {
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::commandList));
        $nb = count($notifications);

        return new Response($nb);
    }
    public function countNotificationCommandeEcommerceAction()
    {
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::commande));
        $nb = count($notifications);

        return new Response($nb);
    }

    public function deleteContributionAction()
    {
        $em = $this->getDoctrine()->getManager();
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::contribution));
        $nombre_notification = count($notifications);
        foreach ($notifications as $notification) {
            $em->remove($notification);
        }
        $em->flush();

        return $this->redirectToRoute('contribution');
    }
    public function deleteCommandeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::commandList));
        $nombre_notification = count($notifications);
        foreach ($notifications as $notification) {
            $em->remove($notification);
        }
        $em->flush();

        return $this->redirectToRoute('contribution');
    }
    public function deleteCommandeEcommerceAction()
    {
        $em = $this->getDoctrine()->getManager();
        $notifications = $this->getDoctrine()->getRepository('MyWeddingCollecteBundle:Notification')->findBy(array('name' => Notification::commande));
        $nombre_notification = count($notifications);
        foreach ($notifications as $notification) {
            $em->remove($notification);
        }
        $em->flush();

        return $this->redirectToRoute('contribution');
    }
}