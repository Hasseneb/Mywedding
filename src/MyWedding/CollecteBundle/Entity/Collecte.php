<?php

namespace MyWedding\CollecteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collecte
 *
 * @ORM\Table(name="collecte")
 * @ORM\Entity
 */
class Collecte
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nameProduit", type="string", length=255)
     */
    private $nameProduit;

    /**
     * @var integer
     *
     * @ORM\ManyToMany(targetEntity="MyWedding\ProduitBundle\Entity\ListCouple",mappedBy="collecte")
     */
    private $list;



    /**
     * @ORM\ManyToMany(targetEntity="MyWedding\ProduitBundle\Entity\Product")
     * @ORM\JoinTable(name="users_phonenumbers")
     *
     */
    private $produit;

    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\UserBundle\Entity\User",inversedBy="collections")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="SommeAtteinte", type="float", nullable=true)
     */
    private $sommeAtteinte = 0;

    /**
     * @ORM\Column(type="float")
     */private $prixProduit;
    /**
     * @ORM\OneToMany(targetEntity="Contribution",mappedBy="collecte")
     */
    private $contributions;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * @param int $listId
     */
    public function setListId($listId)
    {
        $this->listId = $listId;
    }



    public function __construct()
    {
        $this->list = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contributions = new \Doctrine\Common\Collections\ArrayCollection();

    }

    public function __toString()
    {
        return (string) $this->getNameProduit();
    }



    /**
     * Set nameProduit
     *
     * @param string $nameProduit
     * @return Collecte
     */
    public function setNameProduit($nameProduit)
    {
        $this->nameProduit = $nameProduit;

        return $this;
    }


    /**
     * Get nameProduit
     *
     * @return string
     */
    public function getNameProduit()
    {
        return $this->nameProduit;
    }

    /**
     * Set sommeAtteinte
     *
     * @param float $sommeAtteinte
     * @return Collecte
     */
    public function setSommeAtteinte($sommeAtteinte)
    {
        $this->sommeAtteinte = $sommeAtteinte;

        return $this;
    }

    /**
     * Get sommeAtteinte
     *
     * @return float
     */
    public function getSommeAtteinte()
    {
        return $this->sommeAtteinte;
    }



    /**
     * Set produit
     *
     * @param \MyWedding\ProduitBundle\Entity\Product $produit
     * @return Collecte
     */
    public function setProduit(\MyWedding\ProduitBundle\Entity\Product $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Get produit
     *
     * @return \MyWedding\ProduitBundle\Entity\Product
     */
    public function getProduit()
    {
        return $this->produit;
    }








    /**
     * Add contributions
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     * @return Collecte
     */
    public function addContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $contribution->setCollecte($this);
        $this->contributions[] = $contribution;

        return $this;
    }

    /**
     * Remove contributions
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     */
    public function removeContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $this->contributions->removeElement($contribution);
    }

    /**
     * Get contributions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContributions()
    {
        return $this->contributions;
    }

    /**
     * Set user
     *
     * @param \MyWedding\UserBundle\Entity\User $user
     * @return Collecte
     */
    public function setUser(\MyWedding\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MyWedding\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add produit
     *
     * @param \MyWedding\ProduitBundle\Entity\Product $produit
     *
     * @return Collecte
     */
    public function addProduit(\MyWedding\ProduitBundle\Entity\Product $produit)
    {
        $this->produit[] = $produit;

        return $this;
    }

    /**
     * Remove produit
     *
     * @param \MyWedding\ProduitBundle\Entity\Product $produit
     */
    public function removeProduit(\MyWedding\ProduitBundle\Entity\Product $produit)
    {
        $this->produit->removeElement($produit);
    }

    /**
     * Add list
     *
     * @param \MyWedding\ProduitBundle\Entity\ListCouple $list
     *
     * @return Collecte
     */
    public function addList(\MyWedding\ProduitBundle\Entity\ListCouple $list)
    {
        $this->list[] = $list;

        return $this;
    }

    /**
     * Remove list
     *
     * @param \MyWedding\ProduitBundle\Entity\ListCouple $list
     */
    public function removeList(\MyWedding\ProduitBundle\Entity\ListCouple $list)
    {
        $this->list->removeElement($list);
    }

    /**
     * Get list
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getList()
    {
        return $this->list;
    }

    /**
     * Set prixProduit
     *
     * @param float $prixProduit
     *
     * @return Collecte
     */
    public function setPrixProduit($prixProduit)
    {
        $this->prixProduit = $prixProduit;

        return $this;
    }

    /**
     * Get prixProduit
     *
     * @return float
     */
    public function getPrixProduit()
    {
        return $this->prixProduit;
    }
}
