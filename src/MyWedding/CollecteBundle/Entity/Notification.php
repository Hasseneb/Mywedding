<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 06/07/2017
 * Time: 13:02
 */

namespace MyWedding\CollecteBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

/**
 * @ORM\Entity
 * @ORM\Table(name="notification")
 */
class Notification extends Controller
{
    const commandList = 'commandeListe';
    const contribution = 'contribution';
    const commande = 'commande';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     */
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }


}