<?php
namespace MyWedding\CollecteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contribution
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MyWedding\CollecteBundle\Repository\ContributionRepository")
 */
class Contribution
{
    public function __construct()
    {
        $this->createdAt = new \DateTime('now', new \DateTimeZone('Africa/Tunis'));
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\UserBundle\Entity\User",inversedBy="contribution")
     * @ORM\JoinColumn(name="donneur_id", referencedColumnName="id")
     */
    private $donnneur;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private  $nomDonneur;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private  $prenomDonneur;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private  $message;
    /**
     * @ORM\Column(type="boolean")
     */
    private $isReceived = false;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    private $tel;
    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\ProduitBundle\Entity\Payment", inversedBy="");
     */
    private $paymentMethod;

    /**
     * @return mixed
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * @param mixed $tel
     */
    public function setTel($tel)
    {
        $this->tel = $tel;
    }

    /**
     * @return mixed
     */
    public function getNomDonneur()
    {
        return $this->nomDonneur;
    }

    /**
     * @param mixed $nomDonneur
     */
    public function setNomDonneur($nomDonneur)
    {
        $this->nomDonneur = $nomDonneur;
    }

    /**
     * @return mixed
     */
    public function getPrenomDonneur()
    {
        return $this->prenomDonneur;
    }

    /**
     * @param mixed $prenomDonneur
     */
    public function setPrenomDonneur($prenomDonneur)
    {
        $this->prenomDonneur = $prenomDonneur;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getIsReceived()
    {
        return $this->isReceived;
    }

    /**
     * @param mixed $isReceived
     */
    public function setIsReceived($isReceived)
    {
        $this->isReceived = $isReceived;
    }


    /**
     * @ORM\ManyToOne(targetEntity="Collecte",inversedBy="contributions")
     * @ORM\JoinColumn(name="collecte_id", referencedColumnName="id")
     */
    private $collecte;



    /**
     * @var float
     *
     * @ORM\Column(name="somme", type="float",nullable=true)
     */
    private $somme;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set somme
     *
     * @param float $somme
     * @return Contribution
     */
    public function setSomme($somme)
    {
        $this->somme = $somme;

        return $this;
    }

    /**
     * Get somme
     *
     * @return float
     */
    public function getSomme()
    {
        return $this->somme;
    }
    /**
     * Set donnneur
     *
     * @param \MyWedding\UserBundle\Entity\User $donnneur
     * @return Contribution
     */
    public function setDonnneur(\MyWedding\UserBundle\Entity\User $donnneur = null)
    {
        $this->donnneur = $donnneur;

        return $this;
    }

    /**
     * Get donnneur
     *
     * @return \MyWedding\UserBundle\Entity\User 
     */
    public function getDonnneur()
    {
        return $this->donnneur;
    }

    /**
     * Set collecte
     *
     * @param \MyWedding\CollecteBundle\Entity\Collecte $collecte
     * @return Contribution
     */
    public function setCollecte(\MyWedding\CollecteBundle\Entity\Collecte $collecte = null)
    {
        $this->collecte = $collecte;

        return $this;
    }

    /**
     * Get collecte
     *
     * @return \MyWedding\CollecteBundle\Entity\Collecte 
     */
    public function getCollecte()
    {
        return $this->collecte;
    }

    /**
     * Set paymentMethod
     *
     * @param \MyWedding\ProduitBundle\Entity\Payment $paymentMethod
     *
     * @return Contribution
     */
    public function setPaymentMethod(\MyWedding\ProduitBundle\Entity\Payment $paymentMethod = null)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \MyWedding\ProduitBundle\Entity\Payment
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Contribution
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt->format(" j F, Y, g:i a");
    }
}
