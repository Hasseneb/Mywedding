<?php
// src/AppBundle/Entity/User.php

namespace MyWedding\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use MyWedding\ProduitBundle\Entity\Newsletter;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * @var string
     *
     * @ORM\Column(name="facebook_id", type="string", nullable=true)
     */
    private $facebook_id ;

    /**
     * @ORM\OneToOne(targetEntity="MyWedding\ProduitBundle\Entity\Newsletter")
     */private $newsletter;
    /**
     * @var string
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $firstNamePartenaire;
    /**
     * @var string
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $emailPartenaire;

    /**
     * @var string
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $lastNamePartenaire;


    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $firstname;


    const CIVILITY_MRS = 'MRS';
    const CIVILITY_MR = 'MR';

    public static $civilityList = array(
        self::CIVILITY_MRS => 'Madame',
        self::CIVILITY_MR => 'Monsieur',
    );
    /**
     * @var string
     */
    private $sexe;

    /**
     * @var string
     */
    private $lastname;



    /**
     * @ORM\OneToMany(targetEntity="MyWedding\ProduitBundle\Entity\CommandList",mappedBy="user")
     */
    private $commandList;

    /**
     * @ORM\OneToOne(targetEntity="Picture",cascade={"all"})
     * @ORM\JoinColumn(name="picture_id", referencedColumnName="id")
     */
    private $picture;

    /**
     * @ORM\OneToMany(targetEntity="MyWedding\CollecteBundle\Entity\Contribution",mappedBy="donneur")
     */
    private $contribution;


    /**
     * @ORM\OneToMany(targetEntity="MyWedding\CollecteBundle\Entity\Collecte",mappedBy="user")
     */
    private $collections;





    /**
     * @ORM\OneToOne(targetEntity="MyWedding\ProduitBundle\Entity\ListCouple", mappedBy="user")
     */
    private $listeCouple;


    /**
     * @ORM\OneToOne(targetEntity="MyWedding\ProduitBundle\Entity\Adresse", mappedBy="user")
     */
    private $adresse;




    /**
     * @return mixed
     */
    public function getNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param mixed $newsletter
     */
    public function setNewsletter(Newsletter $newsletter =null)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * @return mixed
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * @param mixed $adresse
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;
    }


    /**
     * @return string
     */
    public function getFirstNamePartenaire()
    {
        return $this->firstNamePartenaire;
    }

    /**
     * @param string $firstNamePartenaire
     */
    public function setFirstNamePartenaire($firstNamePartenaire)
    {
        $this->firstNamePartenaire = $firstNamePartenaire;
    }

    /**
     * @return string
     */
    public function getLastNamePartenaire()
    {
        return $this->lastNamePartenaire;
    }

    /**
     * @param string $lastNamePartenaire
     */
    public function setLastNamePartenaire($lastNamePartenaire)
    {
        $this->lastNamePartenaire = $lastNamePartenaire;
    }




    /**
     * @return string
     */
    public function getEmailPartenaire()
    {
        return $this->emailPartenaire;
    }

    /**
     * @param string $emailPartenaire
     */
    public function setEmailPartenaire($emailPartenaire)
    {
        $this->emailPartenaire = $emailPartenaire;
    }








    public function __toString()
    {
        return (string) $this->getUsername();
    }




    /**
     * @return mixed
     */
    public function getListeCouple()
    {
        return $this->listeCouple;
    }

    /**
     * @param mixed $listeCouple
     */
    public function setListeCouple($listeCouple)
    {
        $this->listeCouple = $listeCouple;
    }


    /**
     * @return mixed
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @return string
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }






    /**
     * @param mixed $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }




    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }


    /**
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * @param string $sexe
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;
    }


    /**
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * @param string $facebook_id
     */
    public function setFacebookId($facebook_id)
    {
        $this->facebook_id = $facebook_id;
    }




    public function __construct()
    {
        parent::__construct();
        $this->media = new \Doctrine\Common\Collections\ArrayCollection();

    }



    /**
     * Add contribution
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     * @return User
     */
    public function addContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $this->contribution[] = $contribution;

        return $this;
    }

    /**
     * Remove contribution
     *
     * @param \MyWedding\CollecteBundle\Entity\Contribution $contribution
     */
    public function removeContribution(\MyWedding\CollecteBundle\Entity\Contribution $contribution)
    {
        $this->contribution->removeElement($contribution);
    }

    /**
     * Get contribution
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Add collections
     *
     * @param \MyWedding\CollecteBundle\Entity\Collecte $collections
     * @return User
     */
    public function addCollection(\MyWedding\CollecteBundle\Entity\Collecte $collections)
    {
        $this->collections[] = $collections;

        return $this;
    }

    /**
     * Remove collections
     *
     * @param \MyWedding\CollecteBundle\Entity\Collecte $collections
     */
    public function removeCollection(\MyWedding\CollecteBundle\Entity\Collecte $collections)
    {
        $this->collections->removeElement($collections);
    }

    /**
     * Get collections
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCollections()
    {
        return $this->collections;
    }
}
