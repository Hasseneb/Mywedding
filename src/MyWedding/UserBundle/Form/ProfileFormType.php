<?php
/**
 * Created by PhpStorm.
 * User: PCASUS
 * Date: 17/02/2017
 * Time: 13:13
 */

namespace MyWedding\UserBundle\Form;


use MyWedding\UserBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('firstname', null, array('translation_domain' => 'FOSUserBundle'))
            ->add('lastname')
            ->add('sexe', 'choice', array(
                'choices' => User::$civilityList,))
            ->add('numero')
            ->add('firstNamePartenaire')
            ->add('lastNamePartenaire')
            ->add('emailPartenaire')
            ;
    }

    public function getParent()
    {
        return 'fos_user_profile';
    }

    public function getName()
    {
        return 'app_user_profile';
    }
}