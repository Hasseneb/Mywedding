<?php

namespace MyWedding\UserBundle\Controller;

use MyWedding\ProduitBundle\Entity\Basket;
use MyWedding\ProduitBundle\Entity\ListCouple;
use MyWedding\ProduitBundle\Entity\Picture;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;

use MyWedding\UserBundle\Entity\User;
use MyWedding\UserBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Response;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MyWeddingUserBundle:User')->findAll();

        return $this->render(
            'MyWeddingUserBundle:User:index.html.twig',
            array(
                'entities' => $entities,
            )
        );
    }

    /**
     * Creates a new User entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_show', array('id' => $entity->getId())));
        }

        return $this->render(
            'MyWeddingUserBundle:User:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(
            new UserType(),
            $entity,
            array(
                'action' => $this->generateUrl('user_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        return $this->render(
            'MyWeddingUserBundle:User:new.html.twig',
            array(
                'entity' => $entity,
                'form' => $form->createView(),
            )
        );
    }

    function changerPhotoProfilAction(Request $request)
    {
        $cover2 = new Picture();
        $cover = $request->files->get('profil');

        if ($cover != null) {
            $cover2->setFile($cover);
            $user = $this->getUser();
            $em = $this->getDoctrine()->getManager();
            $user->setPicture($cover2);
            $em->persist($user);
            $em->flush();

        }
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'MyWeddingUserBundle:User:show.html.twig',
            array(
                'entity' => $entity,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MyWeddingUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render(
            'MyWeddingUserBundle:User:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(
            new UserType(),
            $entity,
            array(
                'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MyWeddingUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $picture = $editForm["picture"]->getData();
            $picture->preUpload();
            $entity->setPicture($picture);
            $em->persist($picture);
            $em->flush();

            return $this->redirect($this->generateUrl('user_edit', array('id' => $id)));
        }

        return $this->render(
            'MyWeddingUserBundle:User:edit.html.twig',
            array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a User entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MyWeddingUserBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    public function profAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.context')->getToken()->getUser();
        $cart = $em->getRepository('SalesBundle:Cart')->findListCartUser( $user);

        return $this->render(
            'MyWeddingUserBundle:User:profilUser.html.twig',
            array(
                'entities' => $user,
                'commandes' => $cart
            )
        );
    }

    public function chercherUserAction()
    {

        $finder = $this->container->get('fos_elastica.finder.app.user');

        $results = $finder->find('example.net');
        $pagination = $paginator->paginate($results, $page, 10, $options);
    }


    public function listerProductListAction()
    {


        $user = $this->get('security.context')->getToken()->getUser();
        $id_user = $user->getId();
        $em = $this->getDoctrine()->getManager();
        $liste = $user->getListeCouple();
        $product = $liste->getProducts();


        return $this->render(
            'MyWeddingUserBundle:User:contenuList.html.twig',
            array(
//            'entities' => $entities,
                'product' => $product,
            )
        );


    }
    public function pdflistUserAction()
    {

        $em = $this->getDoctrine()->getManager();
        $date = new \DateTime();
        $entity = $em->getRepository('MyWeddingUserBundle:User')->findAll();
        $myTwig = "pdfUser.html.twig";
        $template = $this->renderView($myTwig, array('tab' => $entity, 'date' => $date));
        $html2pdf = new \Html2Pdf_Html2Pdf('P', 'A4', 'fr', true, 'UTF-8');
        ob_start();
        $html2pdf->WriteHTML($template);
        ob_end_clean();
        $content = $html2pdf->Output('', true);
        $response = new Response();
        $response->setContent($content);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-disposition', 'filename=listUser.pdf');
        return $response;
    }

}
