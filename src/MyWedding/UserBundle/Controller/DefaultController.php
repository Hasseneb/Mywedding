<?php

namespace MyWedding\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MyWeddingUserBundle:Default:index.html.twig');
    }
}
