<?php
/**
 * Created by PhpStorm.
 * User: Oumayma
 * Date: 24/02/2017
 * Time: 10:54
 */

namespace SalesBundle\Repository;
use SalesBundle\Entity\CartSeller;

class CartSellerRepository extends \Doctrine\ORM\EntityRepository
{
    public function statOrderBySeller($seller){
        $query = $this->createQueryBuilder('a')
            ->andWhere('a.createdAt >= :date')
            ->andWhere('a.seller =:seller')
            ->setParameter('date', new \DateTime('-12 month'))
            ->setParameter('seller', $seller);
        return $query->getQuery()->getResult();
    }

    public function NotifByUser($user){
    $query = $this->createQueryBuilder('l')
        ->leftJoin('l.cart' , 'c', "WITH", "c.user=:user")
        ->andWhere('l.status = :status')
        ->orderBy('l.createdAt', 'DESC')
        ->andWhere('l.showed =:showed')
        ->setParameter('showed', false)
        ->setParameter('status', CartSeller::STATUS_ACCEPTED)
        ->setParameter('user', $user);
    return  $query->getQuery()->getResult();
    }

    public function cmdParMois()
    {
        $query =$this->getEntityManager()->createQuery("select count(c) , MONTH(c.createdAt) as month_x from SalesBundle:CartSeller c group by month_x ");
        return $query->getResult();
    }
}