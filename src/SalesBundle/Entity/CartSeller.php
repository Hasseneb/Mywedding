<?php
/**
 * Created by PhpStorm.
 * User: Oumayma
 * Date: 24/02/2017
 * Time: 10:14
 */

namespace SalesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * Cart Entity.
 *
 * @ORM\Table(name="cartseller")
 * @ORM\Entity(repositoryClass="SalesBundle\Repository\CartSellerRepository")
 */
class CartSeller
{
    const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETE = 'complete';
    const STATUS_CANCELED = 'canceled';
    const STATUS_ACCEPTED = 'en cours';
    const STATUS_WAITED = 'commande établie';
    const WAITED_PAIMENT = 'en attende de paiement';
    const ACCEPTED_PAIMENT = 'paiement accepté';
    const SHIPPING = 'livré';

    /**
     * The id of the cart.
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * The creation date of this cart.
     *
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime")
     */
    private $modifiedAt;



    /**
     * @var Cart
     *
     * @ORM\ManyToOne(targetEntity="SalesBundle\Entity\Cart")
     * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
     */
    private $cart;

    /**
     * The status of the order.
     *
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;



    /**
     * The showed of the order.
     *
     * @var string
     *
     * @ORM\Column(name="showed", type="boolean")
     */
    private $showed;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return CartSeller
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set modifiedAt
     *
     * @param \DateTime $modifiedAt
     * @return CartSeller
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Get modifiedAt
     *
     * @return \DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return CartSeller
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }



    /**
     * Set cart
     *
     * @param \SalesBundle\Entity\Cart $cart
     * @return CartSeller
     */
    public function setCart(\SalesBundle\Entity\Cart $cart = null)
    {
        $this->cart = $cart;

        return $this;
    }

    /**
     * Get cart
     *
     * @return \SalesBundle\Entity\Cart 
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * Set showed
     *
     * @param boolean $showed
     * @return CartSeller
     */
    public function setShowed($showed)
    {
        $this->showed = $showed;

        return $this;
    }

    /**
     * Get showed
     *
     * @return boolean 
     */
    public function getShowed()
    {
        return $this->showed;
    }





}
