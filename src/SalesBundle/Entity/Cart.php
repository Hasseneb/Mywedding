<?php

namespace SalesBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use MyWedding\UserBundle\Entity\User;

/**
 * Cart Entity.
 *
 * @ORM\Table(name="cart")
 * @ORM\Entity(repositoryClass="SalesBundle\Repository\CartRepository")
 */
class Cart
{
    const STATUS_PROCESSING = 'processing';
    const STATUS_COMPLETE = 'complete';
    const STATUS_CANCELED = 'canceled';
    const STATUS_ACCEPTED = 'en cours';
    const STATUS_WAITED = 'commande établie';
    const WAITED_PAIMENT = 'en attende de paiement';
    const ACCEPTED_PAIMENT = 'paiement accepté';
    const SHIPPING = 'livré';

    /**
     * The id of the cart.
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * The one-to-one association between User and Cart.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="MyWedding\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="SalesBundle\Entity\AdressLivraison",mappedBy="cart")
     */
    private $adressDeLivraison;

    /**
     * The creation date of this cart.
     *
     * @var DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * The modification date of this cart.
     *
     * @var DateTime
     *
     * @ORM\Column(name="modified_at", type="datetime")
     */
    private $modifiedAt;

    /**
     * The one-to-many association between Cart and CartItem.
     *
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="CartItem", mappedBy="cart",cascade={"remove"})
     */
    private $items;


    /**
     * @ORM\ManyToOne(targetEntity="MyWedding\ProduitBundle\Entity\Payment",inversedBy="cart")
     */
    private $paymentMethod;

    /**
     * The status of the order.
     *
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;


    private $numOrder;

    public function getNumOrder()
    {
        $num = 'myWedding-'.$this->id;
        return $num;
    }
    /**
     * The message.
     *
     * @var string
     *
     * @ORM\Column(name="message", type="string", length=255, nullable=true)
     */
    private $message;

    
    /**
     * Initializes items.
     */
    public function __construct() {
        $this->items = new ArrayCollection;
    }

    /**
     * Gets id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets user.
     *
     * @param User $user
     *
     * @return Cart
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Gets user.
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets creation date.
     *
     * @param DateTime $createdAt
     *
     * @return Cart
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Gets creation date.
     *
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Sets modification date.
     *
     * @param DateTime $modifiedAt
     *
     * @return Cart
     */
    public function setModifiedAt($modifiedAt)
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }

    /**
     * Gets modification date.
     *
     * @return DateTime
     */
    public function getModifiedAt()
    {
        return $this->modifiedAt;
    }

    /**
     * Gets items.
     *
     * @return ArrayCollection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Sets items.
     *
     * @param ArrayCollection $items
     */
    public function setItems(ArrayCollection $items)
    {
        $this->items = $items;
    }

    /**
     * Add items
     *
     * @param \SalesBundle\Entity\CartItem $items
     * @return Cart
     */
    public function addItem(\SalesBundle\Entity\CartItem $items)
    {
        $this->items[] = $items;

        return $this;
    }

    /**
     * Remove items
     *
     * @param \SalesBundle\Entity\CartItem $items
     */
    public function removeItem(\SalesBundle\Entity\CartItem $items)
    {
        $this->items->removeElement($items);
    }


    /**
     * Set message
     *
     * @param string $message
     * @return Cart
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    public function __toString(){
        return(string)$this->getStatus();
    }

    /**
     * Set paymentMethod
     *
     * @param string $paymentMethod
     * @return Cart
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return string 
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * Set status
     *
     * @param string $status
     * @return Cart
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Add adressDeLivraison
     *
     * @param \SalesBundle\Entity\AdressLivraison $adressDeLivraison
     *
     * @return Cart
     */
    public function addAdressDeLivraison(\SalesBundle\Entity\AdressLivraison $adressDeLivraison)
    {
        $this->adressDeLivraison[] = $adressDeLivraison;

        return $this;
    }

    /**
     * Remove adressDeLivraison
     *
     * @param \SalesBundle\Entity\AdressLivraison $adressDeLivraison
     */
    public function removeAdressDeLivraison(\SalesBundle\Entity\AdressLivraison $adressDeLivraison)
    {
        $this->adressDeLivraison->removeElement($adressDeLivraison);
    }

    /**
     * Get adressDeLivraison
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdressDeLivraison()
    {
        return $this->adressDeLivraison;
    }

    /**
     * Add paymentMethod
     *
     * @param \MyWedding\ProduitBundle\Entity\Payment $paymentMethod
     *
     * @return Cart
     */
    public function addPaymentMethod(\MyWedding\ProduitBundle\Entity\Payment $paymentMethod)
    {
        $this->paymentMethod[] = $paymentMethod;

        return $this;
    }

    /**
     * Remove paymentMethod
     *
     * @param \MyWedding\ProduitBundle\Entity\Payment $paymentMethod
     */
    public function removePaymentMethod(\MyWedding\ProduitBundle\Entity\Payment $paymentMethod)
    {
        $this->paymentMethod->removeElement($paymentMethod);
    }
}
