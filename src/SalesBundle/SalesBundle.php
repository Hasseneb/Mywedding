<?php

namespace SalesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use SalesBundle\DependencyInjection\Compiler\ServiceCompilerPass;

/**
 * Adds a compiler pass.
 *
 * @param ContainerBuilder $container
 */
class SalesBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ServiceCompilerPass());
    }
}
