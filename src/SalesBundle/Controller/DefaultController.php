<?php

namespace SalesBundle\Controller;

use SalesBundle\Entity\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SalesBundle:Default:index.html.twig');
    }

// n5arjou les details
    public function ordersAction(Request $request)
    {
        $user = $this->getUser();
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('SalesBundle:Cart')->findBy(['user' => $user]);

        $total[]=array();
        foreach ($cart as $c){
            $items = $c->getItems();
            $cartSubtotal = CartController::calculateTotalPrice($items);
            $total[] = $cartSubtotal;
        }
        return $this->render('SalesBundle:Default/checkout:orders.html.twig', [
            'customer_orders' => $cart,
            'total'=>$total
        ]);
    }
// koll commande mnech metkawna
    public function detailOrderAction($idCmd){
        $user = $this->getUser();
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            throw $this->createAccessDeniedException();
        }

        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('SalesBundle:CartItem')->findBy(['cart'=>$idCmd]);

         $total = 0;
        foreach ($cart as $item) {
            $total = $total + $item->getUnitPrice()*$item->getQty();
        }

        return $this->render('SalesBundle:Default/checkout:detailOrders.html.twig', [
            'cart' => $cart, 'total'=>$total
        ]);
    }
}

