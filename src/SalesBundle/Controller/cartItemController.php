<?php
/**
 * Created by PhpStorm.
 * User: micro
 * Date: 31/07/2017
 * Time: 10:29
 */

namespace SalesBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class cartItemController extends Controller
{
    public function changeQuantityAction(Request $request,$id){
        if ($request->isMethod('POST')){
            $quantite = $request->request->get('quantite');
            $em = $this->getDoctrine()->getManager();
            $cart_item = $em->getRepository('SalesBundle:CartItem')->findOneBy(array('id' => $id));
            $cart_item->setQty($quantite);
            $em->persist($cart_item);
            $em->flush();
        }
        return new Response();

    }
}