<?php

namespace SalesBundle\Controller;

use MyWedding\ProduitBundle\Entity\Product;
use MyWedding\UserBundle\Entity\User;
use DateTime;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use SalesBundle\Entity\Cart;
use SalesBundle\Entity\CartItem;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;

class CartController extends Controller
{
    /**
     * Displays "Shopping Cart" page.
     *
     * @return RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        $user = $this->getUser();
        if ($user)
        {
            $em = $this->getDoctrine()->getManager();
            $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(
                ['user' => $user, 'status' => Cart::STATUS_PROCESSING]
            );
            $items = null;
            $total = 0;
            if ($cart != null) {
                $items = $cart->getItems();
                $total = self::calculateTotalPrice($items);
            }
            return $this->render(
                'SalesBundle:Default:cart/index.html.twig',
                [
                    'items' => $items,
                    'total' => $total,
                ]
            );
        }else{
            $products = $request->getSession()->get('products');
            return $this->render(
                'SalesBundle:Default:cart/index.html.twig',[
                    'products' => $products,
                    'items' => 0,
                    'total' => 0,
                ]
            );
        }


    }

    public function indexAdminAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $carts = $em->getRepository('SalesBundle:Cart')->findListCartAdmin();
        //$cart = $em->getRepository('SalesBundle:Cart')->findOneBy(['user' => $user, 'status' => Cart::STATUS_PROCESSING]);
        /*$items = null;
        $total = 0;
        if ($carts != null) {
            $items = $carts->getItems();
            $total = self::calculateTotalPrice($items);
        }*/

        return $this->render(
            'SalesBundle:Default:cart/indexAdmin.html.twig',
            [
                'carts' => $carts,
            ]
        );
    }

    /**
     * Updates shopping cart.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function updateAction(Request $request)
    {
        $items = $request->get('item');
        $em = $this->getDoctrine()->getManager();
        foreach ($items as $id => $qty) {
            $cartItem = $em->getRepository('SalesBundle:CartItem')->find($id);
            if (intval($qty) > 0) {
                $cartItem->setQty($qty);
                $em->persist($cartItem);
            } else {
                $em->remove($cartItem);
            }
        }
        $em->flush();
        $this->addFlash('success', 'Cart updated.');

        return $this->redirectToRoute('web_shop_sales_cart');
    }

    /**
     * Adds products to shopping cart.
     *
     * @param int $id
     *
     * @return RedirectResponse
     */
    public function addAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('MyWeddingProduitBundle:Product')->find($id);
        if ($user = $this->getUser()) {
            /**
             * @var $product Product
             * @var $user User
             * @var $em EntityManager
             */
            $now = new DateTime;
            $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(
                ['user' => $user, 'status' => Cart::STATUS_PROCESSING]
            );
            $cart = $this->persistCart($user, $now, $em, $cart, Cart::STATUS_PROCESSING);
            $em->flush();
            $cartItem = $em->getRepository('SalesBundle:CartItem')->findOneBy(['cart' => $cart, 'product' => $product]);
            $this->persistCartItem($now, $cart, $product, $em, $cartItem);
            $em->flush();

//            $this->addFlash('success', sprintf('%s successfully added to cart', $product->getName()));

            return $this->redirectToRoute('web_shop_sales_cart');
        } else {
            $session = $request->getSession();
            if ($session->has('products')) {
                $products = $session->get('products');
                array_push($products,$product);
                $session->set('products', $products);
            } //if it doesnt create the session and push a array for testing
            else {
                $products = [];
                array_push($products, $product);
                $session->set('products', $products);
            }
            $referer = $request->headers->get('referer');
            return new RedirectResponse($referer);
        }
    }

    /**
     * Calculates total price.
     *
     * @param CartItem[] $items
     *
     * @return float
     */
    public static function calculateTotalPrice($items)
    {
        $total = null;
        foreach ($items as $item) {
            $total += floatval($item->getQty() * $item->getUnitPrice());
        }

        return $total;
    }

    /**
     * Creates the cart for current user.
     *
     * @param User $user
     * @param DateTime $now
     * @param EntityManager $em
     * @param Cart $cart
     *
     * @return Cart
     */
    private function persistCart(User $user, DateTime $now, EntityManager $em, Cart $cart = null, $status)
    {
        if (!$cart) {
            $cart = new Cart;
            $cart->setUser($user);
            $cart->setCreatedAt($now);
            $cart->setModifiedAt($now);
            $cart->setStatus($status);
        } else {
            $cart->setModifiedAt($now);
        }
        $em->persist($cart);

        return $cart;
    }

    /**
     * Creates the chosen cart item.
     *
     * @param DateTime $now
     * @param Cart $cart
     * @param Product $product
     * @param EntityManager $em
     * @param CartItem $cartItem
     */
    private function persistCartItem(
        DateTime $now,
        Cart $cart,
        Product $product,
        EntityManager $em,
        CartItem $cartItem = null
    ) {
        if (!$cartItem) {
            $cartItem = new CartItem;
            $cartItem->setCart($cart);
            $cartItem->setProduct($product);
            $cartItem->setQty(1);
            if ($product->getPromotion() == false) {
                $cartItem->setUnitPrice($product->getPrix());
            } else {
                $cartItem->setUnitPrice(
                    $product->getPrix() - (($product->getPrix() * $product->getPromotion()->getPercent()) / 100)
                );
            }
            $cartItem->setCreatedAt($now);
            $cartItem->setModifiedAt($now);
        } else {
            $cartItem->setQty($cartItem->getQty() + 1);
            $cartItem->setModifiedAt($now);
        }
        $em->persist($cartItem);
    }

    public function removeItemFromCartAction($idItem)
    {
        $em = $this->getDoctrine()->getManager();
        $cartItem = $em->getRepository('SalesBundle:CartItem')->find($idItem);
        $em->remove($cartItem);
        $em->flush();

        return $this->redirectToRoute('web_shop_sales_cart');
    }


    public function removeItemFromCartAjaxAction(Request $req)
    {
        $em = $this->getDoctrine()->getManager();
        $idItem = $req->get('idItem');
        $cartItem = $em->getRepository('SalesBundle:CartItem')->find($idItem);
        if($cartItem != null) {
            $em->remove($cartItem);
            $em->flush();
        }

        return new JsonResponse(array('data', 1));
    }

    public function editStatusAction($id, $status)
    {

        $em = $this->getDoctrine()->getManager();

        $cart = $em->getRepository('SalesBundle:Cart')->find($id);
        if($cart != null){
            $cart->setStatus($status);
            $em->flush();
        }

        return $this->redirectToRoute('my_Wedding_commande');
    }

    public function showDetailCartAdminAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $cart = $em->getRepository('SalesBundle:Cart')->find($id);

        return $this->render(
            'SalesBundle:Default:cart/detailCmdAdmin.html.twig',
            [
                'cart' => $cart,
            ]
        );
    }

    public function editStatusUserAction($id)
    {

        $em = $this->getDoctrine()->getManager();

        $cart = $em->getRepository('SalesBundle:Cart')->find($id);
        if($cart != null){
            $cart->setStatus(Cart::STATUS_CANCELED);
            $em->flush();
        }

        return $this->redirectToRoute('user_prof');
    }


}