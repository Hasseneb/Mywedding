<?php

namespace SalesBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use MyWedding\CollecteBundle\Entity\Notification;
use MyWedding\UserBundle\Entity\User;
use DateTime;
use SalesBundle\Entity\AdressLivraison;
use SalesBundle\Entity\BonAchat;
use SalesBundle\Entity\Cart;
use SalesBundle\Entity\CartSeller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Length;
use SalesBundle\Entity\CartItem;
use SalesBundle\Entity\SalesOrder;
use SalesBundle\Entity\SalesOrderItem;
class CheckoutController extends Controller
{
    /**
     * Constructs first checkout step gathering the shipment information.
     *
     * @return Response
     */
    public function indexAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(['user' => $user, 'status' => Cart::STATUS_PROCESSING]);
        $items = $cart->getItems();
        $total = CartController::calculateTotalPrice($items);
        $user = $this->container->get('security.context')->getToken()->getUser();
        $paymentMethods = $em->getRepository('MyWeddingProduitBundle:Payment')->findAll();

        return $this->render('SalesBundle:Default:checkout/index.html.twig', [
            'payments' => $paymentMethods,
            'user' => $user,
            'items' => $items,
            'cart_subtotal' => $total,
        ]);
    }


    public function createAdressLivraisonForCheckoutAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(['user' => $user, 'status'=>Cart::STATUS_PROCESSING]);
        $adr = $request->get('adrL');
        $code = $request->get('codeL');
        $pay = $request->get('paysL');
        $tel = $request->get('telL');
        $payment = $request->get('payment');
        $payment = $em->getRepository('MyWeddingProduitBundle:Payment')->findOneBy(['id' => $payment]);
        $entity = new AdressLivraison();
        $entity->setAdress($adr);
        $entity->setZip($code);
        $entity->setCountry($pay);
        $entity->setUser($user);
        $entity->setPhone($tel);
        $cart->addAdressDeLivraison($entity);
        $cart->setPaymentMethod($payment);
        $em->persist($cart);
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('web_shop_sales_checkout_success');
    }

    /**
     * Constructs second checkout step gathering the payment information.
     *
     * This method stores relevant information into session.
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */




    /**
     * Creates an order.
     *
     * Once the POST submission hits the controller, a new order with all of the related
     * items gets created. At the same time, the cart and cart items are cleared. Finally, the
     * customer is redirected to the order success page.
     *
     * @return RedirectResponse
     */
   public function processAction(Request $request)
    {

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $date= new \DateTime();
        $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(['user' => $user, 'status'=>Cart::STATUS_PROCESSING]);
        $msg = $request->get('msg');
        $cart->setMessage($msg);
        $em->flush();
        $items = $cart->getItems();
        foreach ($items as $item) {
            $item->getProduct()->setNbrAdd($item->getProduct()->getNbrAdd()+1);
            $em->flush();
            $sellerCart = $em->getRepository('SalesBundle:CartSeller')->findOneBy(['cart'=>$cart]);
            $em->flush();
            if($sellerCart == null) {
                $cartSeller = new CartSeller();
                $cartSeller->setStatus(CartSeller::STATUS_WAITED);
                $cartSeller->setCart($cart);
                $cartSeller->setCreatedAt($date);
                $cartSeller->setModifiedAt($date);
                $cartSeller->setShowed(0);
                $em->persist($cartSeller);
                $em->flush();

            }
        }
        $cart->setStatus(Cart::STATUS_ACCEPTED);
        $em->flush();
        return $this->redirectToRoute('my_wedding_user_homepage');
    }

    /**
     * Creates the order success page.
     *
     * @return Response
     */
    public function successAction(Request $request)
    {

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(['user' => $user, 'status'=>Cart::STATUS_PROCESSING]);
        $items = $cart->getItems();
        $total = CartController::calculateTotalPrice($items);

            return $this->render('SalesBundle:Default:checkout/success.html.twig', [
                'total' => $total,
                'last_order' => $this->get('session')->get('last_order'),
                'paymentMethod' => $cart->getPaymentMethod()
            ]);
    }






        public function confirmationCmdAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $sellerCart = $em->getRepository('SalesBundle:CartSeller')->findOneBy(array('id' => $id));
        $sellerCart->setStatus('commande confirmée');
        $notification = new Notification(Notification::commande);
        $em->persist($notification);
        $em->persist($sellerCart);
        $em->flush();

        return new JsonResponse(array('data'=>1));

    }

    public function fraisLivraisonVilleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ville = $request->get('ville');
        $adr = $em->getRepository('cadicomBundle:AdressLivraison')->find($ville);
        $entity = $em->getRepository('cadicomBundle:MethodLivraison')->findOneBy(['city'=>$adr->getCountry()]);
        return new JsonResponse(array('city'=>$entity->getCity(), 'id'=>$entity->getId(), 'price'=>$entity->getPrice()));
    }

    public function fraisLivraisonFactVilleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $ville = $request->get('ville');
        $user = $this->getUser();
        $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(['user' => $user, 'status'=>Cart::STATUS_PROCESSING]);
        $adr = $cart->getAdrLivraison();
        $entity = $em->getRepository('cadicomBundle:MethodLivraison')->findOneBy(['city'=>$adr->getCountry()]);
        return new JsonResponse(array('city'=>$entity->getCity(), 'id'=>$entity->getId(), 'price'=>$entity->getPrice()));
    }

    public function cartInLayoutAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.context')->getToken()->getUser();
        $nbr = 0;
        $total = 0;
        $items = null;
        if ($user instanceof UserInterface) {
            $cart = $em->getRepository('SalesBundle:Cart')->findOneBy(['user' => $user, 'status' => Cart::STATUS_PROCESSING]);
            if ($cart != null) {
                $items = $cart->getItems();
                $total = CartController::calculateTotalPrice($items);
                foreach ($items as $item) {
                    $nbr = $nbr + $item->getQty();
                }
            }
            return $this->render('SalesBundle:Default:cart/cartIcone.html.twig', array('nbrItems' => $nbr, 'totalPrice' => $total, 'items' => $items));
        }else{
            if ($request->getSession()->has('products'))
            {
                $products = $request->getSession()->get('products');
                return $this->render('SalesBundle:Default:cart/cartIcone.html.twig',[
                    'products' => $products
                ]);
            }
            return $this->render('SalesBundle:Default:cart/cartIcone.html.twig', array('nbrItems' => $nbr, 'totalPrice' => $total, 'items' => $items));
        }


    }
}
