<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
$loader->add('Twilio\Rest', __DIR__.'/../vendor/twilio/sdk');
$loader->add('Html2Pdf_', __DIR__.'/../vendor/html2pdf/lib');
return $loader;
