



/*global $, alert, console, noUiSlider*/

window.onbeforeunload = function () {
    window.scrollTo(0, 0);
}

$(document).ready(function () {
    
    "use strict";
    
/* 
    
    $(window).scroll( function() {
        
        var windowScroll = $(window).scrollTop(),
            documentHeight = $(document).height(),
            windowHeight = $(window).height(),
            filterHeight = $(".filter-div").height(),
            filterScroll = $(".filter-div").offset().top,
            filterOffsetBottom = filterHeight + filterScroll,
            breakPoint = filterHeight - windowScroll;

        $(window).bind('scroll', function(event) {
            
        if ($(this).scrollTop() < $(this).data('last-scroll')) {
                $(".filter-div").css({
                    bottom: 'auto',
                    position: 'absolute',
                    top: filterScroll
                });
                if (filterScroll < 300) {
                    $(".filter-div").css({
                        bottom: 'auto',
                        position: 'fixed',
                        top: '210px'
                    });
                }
            }
            else {
                if (windowScroll >= breakPoint) {
                    $(".filter-div").css({
                        bottom: '-30px',
                        position: 'fixed',
                        top: 'auto'
                    });
                }else {
                    $(".filter-div").css({
                        bottom: 'auto',
                        position: 'absolute',
                        top: '210px'
                    }); 
                    console.log($(document).scrollTop() + windowHeight);     console.log($(".brands").offset().top);
                }if ($(document).scrollTop() + windowHeight > $(".brands").offset().top){
                    $(".filter-div").css({
                        bottom: 'auto',
                        position: 'absolute',
                        top: filterScroll
                    }); 
                }
            }
            $(this).data('last-scroll',$(this).scrollTop());
        });
    });*/
    
    var viewportWidth = $(window).width();
    var $this, ink, d, x, y;
    
    $(".rippler").click(function (e) {
        $this = $(this);
        //create .ink element if it doesn't exist
        if ($this.find(".ink").length == 0) {
            $this.prepend("<span class='ink'></span>");
        }

        ink = $this.find(".ink");
        //incase of quick double clicks stop the previous animation
        ink.removeClass("animate");

        //set size of .ink
        if (!ink.height() && !ink.width()) {
            //use $this's width or height whichever is larger for the diameter to make a circle which can cover the entire element.
            d = Math.max($this.outerWidth(), $this.outerHeight());
            ink.css({height: d, width: d});
        }

        //get click coordinates
        //logic = click coordinates relative to page - $this's position relative to page - half of self height/width to make it controllable from the center;
        x = e.pageX - $this.offset().left - ink.width() / 2;
        y = e.pageY - $this.offset().top - ink.height() / 2;

        //set the position and add class .animate
        ink.css({top: y + 'px', left: x + 'px'}).addClass("animate");
		
		setTimeout(function () {
            $this.find(".ink").remove();
		}, 500);
    });
    
    
    if ((viewportWidth) > 768) {
        $('.secondary-nav').affix({
            offset: {
                top: 150/*$('.secondary-nav').offset().top*/
            }
        }).on('affix.bs.affix', function () {
            $('.primary-nav').addClass("undisplayed");
            $('.secondary-nav').addClass("attached");
            $('.ccm').css("top", "600px");
        }).on('affix-top.bs.affix', function () {
            $('.primary-nav').removeClass("undisplayed");
            $('.secondary-nav').removeClass("attached");
            $('.ccm').css("top", "600px");
        });
        // Checking on page load: is [.navbar] affixed?
        if ($('.secondary-nav').hasClass('affix')) {
            $('.primary-nav').addClass("undisplayed");
            $('.secondary-nav').addClass("attached");
        }
    }

    if ((viewportWidth) < 768) {
        $(".search-icon").click(function () {
            $(this).next().css("transform", "scaleX(1)");
            $(this).next().focus();
        });
        $(".form-control").blur(function () {
            $(this).css({"transform" : "scaleX(0)", "background" : "transparent"});
        });
    }
    
    // Text input
    
    $('.text-field-input, .textarea-input').focus(function () {
        $(this).parent().addClass('focused has-label');
    });
    
    $('.text-field-input, .textarea-input').blur(function () {
        
        if ($(this).val() == '') {
            $(this).parent().removeClass('has-label');
        }
        $(this).parent().removeClass('focused');
    });
    
    // Date input
    
    $('.date').blur(function () {
        if ($(this).val() != '') {
            $(this).parent().addClass('showdateback');
        } else {
            $(this).parent().removeClass('showdateback');
        }
    });
    
    // Password input
    

    
    $(".show-pass").hide();

    $("input[type=password]").focus(function () {
        
        var $this = $(this),
        $showpass = $(this).next();
        
        $this.on("keyup",function(){
        if($(this).val())
            $showpass.show();
        else
            $showpass.hide();
        });

        $this.next().mousedown(function(){
            $this.attr('type','text');
        }).mouseup(function(){
            $this.attr('type','password');
            $this.focus();
        }).mouseout(function(){
            /*$(this).hide();*/
            $this.attr('type','password');
        });
        
    });

   
/*
    $("input[type=password]").on("blur",function(){
        setTimeout(function () {
            $(this).next().hide();
        }, 500);
    });    
    $("input[type=password]").on("focus",function(){
        if($(this).val())
        $(this).next().show();
    });*/
    


    // Material select
    
    var materialSelectHover = 0;
    
    $(".material-select").each(function () {
        var $input = $(this).find("input"),
            $inputval = $('.myOptions').val(),
            $ul = $(this).find("> ul"),
            $ulDrop = $ul.clone().addClass("material-select-drop");
        $(this).append('<i class="material-icons">arrow_drop_down</i>', $ulDrop).on({hover : function () {
            materialSelectHover ^= 1;
        }, click : function () {
            $ulDrop.toggleClass("show");
        }
                });
        
        // PRESELECT
        
        $ul.add($ulDrop).find("li[data-value='" + $input.val() + "']").addClass("selected");
        
        // MAKE SELECTED
        
        $ulDrop.on("click", "li", function (evt) {
            evt.stopPropagation();
            $input.val($(this).data("value")); // Update hidden input value
            $ul.find("li").eq($(this).index()).add(this).addClass("selected").siblings("li").removeClass("selected");
        });
        
        // UPDATE LIST SCROLL POSITION
        
        $ul.on("click", function () {
            var liTop = $ulDrop.find("li.selected").position().top;
            $ulDrop.scrollTop(liTop + $ulDrop[0].scrollTop);
        });
    });
    
    // CLOSE ALL OPNED
    
    $(document).on("mouseup", function () {
        if (!materialSelectHover) {
            $(".material-select-drop").removeClass("show");
        }
    });
    
    /*$('.selectedone').on("click", function () {
        alert($('.myOptions').val());
    });*/
    
    $(".rippler").click(function () {
        event.preventDefault();
    });
    
    $(".swatch").click(function () {
        return false;
    });

    
    $(".collapsing-btn").click(function () {
        var i = $(this).find("i"),
            res = i.css("transform");
        if (res != "matrix(6.12323e-17, 1, -1, 6.12323e-17, 0, 0)") {
            i.parent().addClass("clicked-collapse");
        } else {
            i.parent().removeClass("clicked-collapse");
        }
    });
    
    $(".product-details .nav-tabs li").click(function () {
        var highlighter = $(this).parent().find(".highlighter"),
            activePos = $(this).position();
        highlighter.css({
            left: activePos.left,
            width: $(this).width()
        });
    });

    
    $(".quantity i").click(function () {
        
        var qtSpan = $(this).parent().find("span"),
            totSpan = $(this).parent().siblings(".total-price").find("span:first-of-type"),
            unitSpan = $(this).parent().siblings(".unit-price").find("span:first-of-type"),
            unit = unitSpan.html(),
            quantity = qtSpan.html(),
            total = totSpan.html();
        
        if ($(this).hasClass("minus")) {
            if (quantity >= 2) {
                quantity--;
                qtSpan.html(quantity);
                total = quantity * unit;
                totSpan.html(total + " ");
                qtSpan.css({"color": "#00bfe8"});
                setTimeout(function () {
                    qtSpan.css({"color": "#333"});
                }, 500);
            }
        } else {
            quantity++;
            qtSpan.html(quantity);
            total = quantity * unit;
            totSpan.html(total + " ");
            qtSpan.css({"color": "#00bfe8"});
            setTimeout(function () {
                qtSpan.css({"color": "#333"});
            }, 500);
        }
    });
    
    var cartItemsNb = $(".cart-item").length;
    $(".cart .nb-item").html(cartItemsNb);
    
    $(".cart .delete").click(function () {
        var item = $(this).parent().parent();
        cartItemsNb--;
        $(".cart .nb-item").html(cartItemsNb);
        item.hide(500);
        item.addClass("deleted-item");
    });

});